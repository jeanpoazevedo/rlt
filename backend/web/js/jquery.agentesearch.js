(function ($) {
    var Agente = function ($element, options) {
        this.init($element, options);
    };

    Agente.prototype = {
        constructor: Agente,
        init: function ($element, options) {
            this.$element = $element;
            this.$root = this.$element.parent().parent();
            this.$window = this.$root.find('.modal');
            this.action = options.action;
            this.fields = options.fields;
            this.queryParam = options.queryParam;

            var that = this;
            this.$element.parent().find('span a:first').on('click', function () {
                that.search($(this), true);
            });
            this.$root.find('.modal span a:first').on('click', function () {
                that.search($(this), false);
            });
        },
        _assign: function (data) {
            if (!$.isEmptyObject(data)) {
                this.$element.trigger($.Event('beforeAssignData'), [data]);
                for (var prop in this.fields) {
                    var $input = jQuery('#' + this.fields[prop]);
                    if ($input.length) {
                        if (prop in data) {
                            $input.val(data[prop]);
                        } else {
                            $input.val('');
                        }
                    }
                }
                this.$element.trigger($.Event('afterAssignData'), [data]);
            }
        },
        _locate: function (data) {
            var $tbody = this.$window.find('tbody'),
                    that = this;

            $tbody.empty();
            for (var i in data) {
                var row = data[i];
                var tr = $(
                        '<tr>' +
                            '<td><a href="#">' + row.cpf_cnpj + '</a></td>' +
                            '<td>' + row.nome + '</td>' + 
                            '<td>' + row.fantasia + '</td>' +
                            '<td>' + row.endereco + '</td>' + 
                            '<td>' + row.municipio + '</td>' +
                        '</tr>'
                        ).appendTo($tbody);
                tr.data('agente', row);
            }

            $tbody.find('a').on('click', function () {
                var agente = $(this).parent().parent().data('agente');
                that._assign(agente);
                that.$element.find('input:first').val(agente.cpf_cnpj);
                that.$window.modal('hide');
                $('#modalAgente').modal();
            });
        },
        search: function ($button, cpf_cnpj) {
            var $input = cpf_cnpj ? this.$element : this.$window.find('input:first'),
                    val = $input.val(),
                    params = {},
                    that = this;

            if (cpf_cnpj) {
                if (!val) {
                    var modal = this.$window.data("modal");
                    if (!modal) {
                        this.$window.modal({keyboard: false});
                    }
                    this.$window.modal('show');
                    return;
                }
                val = val.replace(/[^0-9]/g, '');
            }

            if (val) {
                params[that.queryParam] = val;
                $.get(this.action, params, function (data) {
                    if (cpf_cnpj) {
                        that._assign(data[0]);
                        $('#modalAgente').modal();
                    } else {
                        that._locate(data);
                    }
                }).fail(function (xhr, status, text) {
                    var error = xhr.responseText;
                    window.alert((error.hasOwnProperty('message') && error.message) ? error.message : text);
                });
            }
        }
    };

    $.fn.agente = function (options) {
        return this.each(function () {
            var $this = $(this),
                    data = $this.data('agente');

            if (!data) {
                $this.data('agente', new Agente($this, options));
            }
        });
    };

})(jQuery);
