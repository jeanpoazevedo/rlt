<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\components\SendMail;
use common\models\Usuario;
use common\models\UsuarioPerfil;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;

class UsuarioController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'password'],
                'rules' => AccessRulesControl::getRulesControl('usuario'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Usuario();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlUsuarioSearch', Yii::$app->request->url);
        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes do Usuario',
                    'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCreate() {
        $model = new Usuario();
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        $model_perfil = new UsuarioPerfil();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model->senha = Yii::$app->security->generatePasswordHash($model->senha);
                $model->senha_hash = $model->senha;
                $model->unidade_temp_fk = $model->unidade_fk;
                $model->senha = Yii::$app->security->generatePasswordHash(date('Ymd'));
                $model->senha_hash = $model->senha;
                $model->auth_key = Yii::$app->security->generateRandomString();
                $model->token = Yii::$app->security->generateRandomString();
                $model->data_asterisk = date('d/m/Y');
                $model->data_rlt = date('d/m/Y');
                if ($model->save()) {
                    $model_perfil = new UsuarioPerfil();
                    $model_perfil->load(Yii::$app->request->post());
                    $model_perfil->usuario_fk = $model->id;
                    if ($model_perfil->save()) {
                        Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                        return $this->redirect(['usuario/index']);
                    }
                }
            } else {
                $model->status = true;
            }
        }
        return $this->render('_form', [
            'title' => 'Adicionar novo Usuario',
            'model' => $model,
            'model_perfil' => $model_perfil,
            'usuarioperfil' => $usuarioperfil,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->senha = '';
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        $usuarioperfil_id = UsuarioPerfil::find()->where(['usuario_fk' => $model->id])->one();

        $usuarioperfilid = $usuarioperfil_id->id;
        
        $model_perfil = $this->findModelusuarioperfil($usuarioperfilid);
        
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model->senha = Yii::$app->security->generatePasswordHash($model->senha);
                $model->senha_hash = $model->senha;
                if ($model->save()) {
                    $model_perfil->load(Yii::$app->request->post());
                    if ($model_perfil->save()) {
                        Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                        return $this->redirect(['usuario/index']);
                    }
                }
            }
        }
        return $this->render('_form', [
            'title' => 'Alterar Usuario',
            'model' => $model,
            'model_perfil' => $model_perfil,
            'usuarioperfil' => $usuarioperfil,
        ]);
    }

    public function actionDelete($id) {
        try {
            $model = $this->findModel($id);
            $model->status = 0;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
            } else {
                Yii::$app->session->setFlash('error', 'Não foi possivel remover!');
            }
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlUsuarioSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }

    protected function findModelusuarioperfil($usuarioperfilid) {
        if (($model_perfil = UsuarioPerfil::findOne($usuarioperfilid)) !== null) {
            return $model_perfil;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionPassword($id) {
        $model = Usuario::findOne($id);
        $model->token = Yii::$app->security->generateRandomString();
        $model->save();
        $emails = [ $model->email ];
        SendMail::submitpassword($emails, $model, 'email_usuario_senha-html');
        Yii::$app->session->setFlash('success', 'Enviado link para atualizar a senha!');
        return $this->redirect(['usuario/index']);
    }

}
