<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\components\Setup;
use common\models\Cdr;
use common\models\Usuario;
use yii\data\ActiveDataProvider;
use yii2tech\spreadsheet\Spreadsheet;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use kartik\date\DatePicker;

class OnlineController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'clean'],
                'rules' => AccessRulesControl::getRulesControl('online'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Cdr();
        $ModelUsuario = Usuario::find()->where(['id' => Yii::$app->user->identity->id])->one();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlOnlineSearch', Yii::$app->request->url);
        
        if (Yii::$app->user->identity->hora_inicio != NULL){
            $hora_inicio = ' '.Yii::$app->user->identity->hora_inicio.':00:00';
        } else if (Yii::$app->user->identity->hora_inicio === 0){
            $hora_inicio = ' 00:00:00';
        } else {
            $hora_inicio = ' 00:00:00';
        }
        if (Yii::$app->user->identity->hora_fim != NULL){
            $hora_fim = ' '.Yii::$app->user->identity->hora_fim.':59:59';
        } else if (Yii::$app->user->identity->hora_fim === 0){
            $hora_fim = ' 00:59:59';
        } else {
            if (Yii::$app->user->identity->hora_inicio != NULL){
                $hora_fim = ' '.Yii::$app->user->identity->hora_inicio.':59:59';
            } else if (Yii::$app->user->identity->hora_inicio === 0){
                $hora_fim = ' 00:59:59';
            } else {
                $hora_fim = ' 23:59:59';
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_total = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->all();
            } else {
                $contageligacoes_total = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->all();
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_total = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->all();
            } else {
                $contageligacoes_total = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->all();
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_atendidas = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all();
            } else {
                $contageligacoes_atendidas = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all();
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_atendidas = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all();
            } else {
                $contageligacoes_atendidas = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all();
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_abandonatas = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'NO ANSWER'])
                        ->all();
            } else {
                $contageligacoes_abandonatas = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'NO ANSWER'])
                        ->all();
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_abandonatas = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'NO ANSWER'])
                        ->all();
            } else {
                $contageligacoes_abandonatas = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'NO ANSWER'])
                        ->all();
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_ocupado = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all();
            } else {
                $contageligacoes_ocupado = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all();
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_ocupado = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all();
            } else {
                $contageligacoes_ocupado = Cdr::find()->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all();
            }
        }
        return $this->render('index', [
            'model' => $searchModel,
            'modelusuario' => $ModelUsuario,
            'dataProvider' => $dataProvider,
            'contageligacoes_total' => $contageligacoes_total,
            'contageligacoes_atendidas' => $contageligacoes_atendidas,
            'contageligacoes_abandonatas' => $contageligacoes_abandonatas,
            'contageligacoes_ocupado' => $contageligacoes_ocupado,
        ]);
    }
    
    protected function findModel($id) {
        if (($model = Cdr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionSetSessionData() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->data_asterisk = $request['data_asterisk'];
            $model->save();
        }
        Yii::$app->user->identity->data_asterisk = $request['data_asterisk'];
    }
    
    public function actionSetSessionVoiporigem() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->voip_origem = $request['voip_origem'];
            $model->save();
        }
        Yii::$app->user->identity->voip_origem = $request['voip_origem'];
    }
    
    public function actionSetSessionVoipdestino() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->voip_destino = $request['voip_destino'];
            $model->save();
        }
        Yii::$app->user->identity->voip_destino = $request['voip_destino'];
    }
    
    public function actionSetSessionHorainicio() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->hora_inicio = $request['hora_inicio'];
            $model->save();
        }
        Yii::$app->user->identity->hora_inicio = $request['hora_inicio'];
    }
    
    public function actionSetSessionHorafim() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->hora_fim = $request['hora_fim'];
            $model->save();
        }
        Yii::$app->user->identity->hora_fim = $request['hora_fim'];
    }
    
    public function actionSetSessionStatuschamada() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->status_chamada = $request['status_chamada'];
            $model->save();
        }
        Yii::$app->user->identity->status_chamada = $request['status_chamada'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->voip_origem = NULL;
            $model->voip_destino = NULL;
            $model->hora_inicio = NULL;
            $model->hora_fim = NULL;
            $model->status_chamada = NULL;
            $model->save();
        }
        return $this->redirect(['online/index']);
    }

}
