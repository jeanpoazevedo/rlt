<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\components\Setup;
use common\models\Cdr;
use common\models\Usuario;
use common\models\UsuarioPerfil;
use common\models\Fila;
use common\models\Queue;
use common\models\QueueAgents;
use yii\data\ActiveDataProvider;
use yii2tech\spreadsheet\Spreadsheet;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

class FilaController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'clean', 'filtro', 'export'],
                'rules' => AccessRulesControl::getRulesControl('fila'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Fila();
        
        $ModelUsuario = Usuario::find()->distinct()->where(['id' => Yii::$app->user->identity->id])->one();
        $model_queue = Queue::find()->where(['id' => Yii::$app->user->identity->fila_destino])->one();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlFilaSearch', Yii::$app->request->url);
        
        if (Yii::$app->user->identity->hora_inicio != NULL){
            $hora_inicio = ' '.Yii::$app->user->identity->hora_inicio.':00:00';
        } else if (Yii::$app->user->identity->hora_inicio === 0){
            $hora_inicio = ' 00:00:00';
        } else {
            $hora_inicio = ' 00:00:00';
        }
        if (Yii::$app->user->identity->hora_fim != NULL){
            $hora_fim = ' '.Yii::$app->user->identity->hora_fim.':59:59';
        } else if (Yii::$app->user->identity->hora_fim === 0){
            $hora_fim = ' 00:59:59';
        } else {
            if (Yii::$app->user->identity->hora_inicio != NULL){
                $hora_fim = ' '.Yii::$app->user->identity->hora_inicio.':59:59';
            } else if (Yii::$app->user->identity->hora_inicio === 0){
                $hora_fim = ' 00:59:59';
            } else {
                $hora_fim = ' 23:59:59';
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_total = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->all());
            } else {
                $contageligacoes_total = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->all());
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_total = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->all());
            } else {
                $contageligacoes_total = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->all());
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_atendidas = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all());
            } else {
                $contageligacoes_atendidas = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all());
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_atendidas = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all());
            } else {
                $contageligacoes_atendidas = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all());
            }
        }
        
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_ocupado = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all());
            } else {
                $contageligacoes_ocupado = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all());
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_ocupado = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all());
            } else {
                $contageligacoes_ocupado = count(Fila::find()->select(['calldate','src'])->distinct()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all());
            }
        }
        
        $contageligacoes_abandonatas = $contageligacoes_total - $contageligacoes_atendidas;
        
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $maxima_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->max('atraso');
            } else {
                $maxima_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->max('atraso');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $maxima_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->max('atraso');
            } else {
                $maxima_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->max('atraso');
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $media_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->average('atraso');
            } else {
                $media_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->average('atraso');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $media_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->average('atraso');
            } else {
                $media_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->average('atraso');
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $minima_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['>', 'atraso', 0])
                        ->min('atraso');
            } else {
                $minima_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['>', 'atraso', 0])
                        ->min('atraso');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $minima_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['>', 'atraso', 0])
                        ->min('atraso');
            } else {
                $minima_atraso = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['>', 'atraso', 0])
                        ->min('atraso');
            }
        }
        
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $maxima_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->max('billsec');
            } else {
                $maxima_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->max('billsec');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $maxima_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->max('billsec');
            } else {
                $maxima_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->max('billsec');
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $media_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->average('billsec');
            } else {
                $media_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->average('billsec');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $media_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->average('billsec');
            } else {
                $media_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->average('billsec');
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $minima_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->andWhere(['>', 'billsec', 0])
                        ->min('billsec');
            } else {
                $minima_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->andWhere(['>', 'billsec', 0])
                        ->min('billsec');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $minima_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->andWhere(['>', 'billsec', 0])
                        ->min('billsec');
            } else {
                $minima_conversa = Fila::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->andWhere(['>', 'billsec', 0])
                        ->min('billsec');
            }
        }
        
        return $this->render('index', [
            'model' => $searchModel,
            'modelusuario' => $ModelUsuario,
            'dataProvider' => $dataProvider,
            'modelqueue' => $model_queue,
            'contageligacoes_total' => $contageligacoes_total,
            'contageligacoes_atendidas' => $contageligacoes_atendidas,
            'contageligacoes_abandonatas' => $contageligacoes_abandonatas,
            'contageligacoes_ocupado' => $contageligacoes_ocupado,
            'maxima_atraso' => $maxima_atraso,
            'media_atraso' => $media_atraso,
            'minima_atraso' => $minima_atraso,
            'maxima_conversa' => $maxima_conversa,
            'media_conversa' => $media_conversa,
            'minima_conversa' => $minima_conversa,
        ]);
    }
    
    protected function findModel($id) {
        if (($model = Fila::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionSetSessionData() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->data_asterisk = $request['data_asterisk'];
            $model->save();
        }
        Yii::$app->user->identity->data_asterisk = $request['data_asterisk'];
    }
    
    public function actionSetSessionVoiporigem() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->voip_origem = $request['voip_origem'];
            $model->save();
        }
        Yii::$app->user->identity->voip_origem = $request['voip_origem'];
    }
    
    public function actionSetSessionVoipdestino() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->voip_destino = $request['voip_destino'];
            $model->save();
        }
        Yii::$app->user->identity->voip_destino = $request['voip_destino'];
    }
    
    public function actionSetSessionHorainicio() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->hora_inicio = $request['hora_inicio'];
            $model->save();
        }
        Yii::$app->user->identity->hora_inicio = $request['hora_inicio'];
    }
    
    public function actionSetSessionHorafim() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->hora_fim = $request['hora_fim'];
            $model->save();
        }
        Yii::$app->user->identity->hora_fim = $request['hora_fim'];
    }
    
    public function actionSetSessionStatuschamada() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->status_chamada = $request['status_chamada'];
            $model->save();
        }
        Yii::$app->user->identity->status_chamada = $request['status_chamada'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->voip_origem = NULL;
            $model->voip_destino = NULL;
            $model->hora_inicio = NULL;
            $model->hora_fim = NULL;
            $model->status_chamada = NULL;
            $model->save();
        }
        return $this->redirect(['fila/index']);
    }
    
    public function actionSetSessionFiladestino() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->fila_destino = $request['fila_destino'];
            $model->save();
        }
        Yii::$app->user->identity->fila_destino = $request['fila_destino'];
        
    }

    public function actionFiltro() {
        $model_fila_delela = Fila::find()
                ->andwhere(['usuario_fk' => Yii::$app->user->identity->id])
                ->andwhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])
                ->all();

        foreach($model_fila_delela as $array_fila_delela){
            $this->findModel($array_fila_delela->id)->delete();
        }
    
        if (Yii::$app->user->identity->hora_inicio != NULL){
            $hora_inicio = ' '.Yii::$app->user->identity->hora_inicio.':00:00';
        } else if (Yii::$app->user->identity->hora_inicio === 0){
        $hora_inicio = ' 00:00:00';
        } else {
            $hora_inicio = ' 00:00:00';
        }
        if (Yii::$app->user->identity->hora_fim != NULL){
            $hora_fim = ' '.Yii::$app->user->identity->hora_fim.':59:59';
        } else if (Yii::$app->user->identity->hora_fim === 0){
            $hora_fim = ' 00:59:59';
        } else {
            if (Yii::$app->user->identity->hora_inicio != NULL){
                $hora_fim = ' '.Yii::$app->user->identity->hora_inicio.':59:59';
            } else if (Yii::$app->user->identity->hora_inicio === 0){
                $hora_fim = ' 00:59:59';
            } else {
        $hora_fim = ' 23:59:59';
            }
        }
        
        $model_queue = Queue::find()->where(['id' => Yii::$app->user->identity->fila_destino])->one();
        
        $model_queue_agents = ArrayHelper::getColumn(
            QueueAgents::find()
            ->where(['queue_fk' => Yii::$app->user->identity->fila_destino])->all(),
            function ($agents){
                return $agents['agents'];
            }
        );
        
        $busca_filas = Cdr::find()
            ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
            ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
            ->andWhere(['=', 'dst', $model_queue->queue])
            ->andWhere(['!=', 'dst', 's'])
            ->orderBy(['calldate' => SORT_ASC])
            ->all();
        foreach ($busca_filas as $key_filas => $value_filas) {
            $busca_fila_ramal = Cdr::find()->distinct()
                    ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                    ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                    ->andWhere(['!=', 'dst', $model_queue->queue])
                    ->andWhere(['!=', 'dst', 's'])
                    ->andWhere(['!=', 'dst', 't'])
                    ->andWhere(['in', 'dst', $model_queue_agents])
                    ->andWhere(['=', 'src', $value_filas->src])
                    ->orderBy(['calldate' => SORT_ASC])
                    ->all();
            foreach ($busca_fila_ramal as $key_fila_ramal => $value_fila_ramal) {
                
                $atraso = $value_fila_ramal->duration - $value_fila_ramal->billsec;
                
                $model = new Fila();
                
                $model->calldate = $value_fila_ramal->calldate;
                $model->src = $value_fila_ramal->src;
                $model->dst = $value_fila_ramal->dst;
                $model->atraso = $atraso;
                $model->billsec = $value_fila_ramal->billsec;
                $model->duration = $value_fila_ramal->duration;
                $model->disposition = $value_fila_ramal->disposition;
                $model->usuario_fk = Yii::$app->user->identity->id;
                $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
                $model->save();
            }
        }
        
        return $this->redirect(['fila/index']);
    }
    
    public function actionExport() {
        $searchModel = new Fila();
        
        $export = new Spreadsheet([
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
            'columns' => [
                'calldate',
                'src',
                'dst',
                [
                    'attribute' => 'atraso',
                    'value' => function ($data) {
                        return gmdate("H:i:s", $data->atraso);
                    },
                ],
                [
                    'attribute' => 'billsec',
                    'value' => function ($data) {
                        return gmdate("H:i:s", $data->billsec);
                    },
                ],
                [
                    'attribute' => 'duration',
                    'value' => function ($data) {
                        return gmdate("H:i:s", $data->duration);
                    },
                ],
                [
                    'attribute' => 'disposition',
                    'value' => function ($data) {
                        switch ($data->disposition) {
                            case 'NO ANSWER':
                                $status = 'Não atendida';
                                break;
                            case 'ANSWERED':
                                $status = 'Atendida';
                                break;
                            case 'BUSY':
                                $status = 'Ocupado';
                                break;
                            default:
                                $status = $data->disposition;
                        }
                        return $status;
                    },
                ],
            ],
        ]);
        return $export->send('Fila.xls');
    }

}
