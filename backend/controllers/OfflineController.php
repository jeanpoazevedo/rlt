<?php

namespace backend\controllers;

use Yii;
use common\components\AccessRulesControl;
use common\components\Setup;
use common\models\Cdr;
use common\models\Voip;
use common\models\Usuario;
use yii\data\ActiveDataProvider;
use yii2tech\spreadsheet\Spreadsheet;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use kartik\date\DatePicker;

class OfflineController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'clean', 'filtro', 'export'],
                'rules' => AccessRulesControl::getRulesControl('offline'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Voip();
        $ModelUsuario = Usuario::find()->where(['id' => Yii::$app->user->identity->id])->one();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlOnlineSearch', Yii::$app->request->url);
        
        if (Yii::$app->user->identity->hora_inicio != NULL){
            $hora_inicio = ' '.Yii::$app->user->identity->hora_inicio.':00:00';
        } else if (Yii::$app->user->identity->hora_inicio === 0){
            $hora_inicio = ' 00:00:00';
        } else {
            $hora_inicio = ' 00:00:00';
        }
        if (Yii::$app->user->identity->hora_fim != NULL){
            $hora_fim = ' '.Yii::$app->user->identity->hora_fim.':59:59';
        } else if (Yii::$app->user->identity->hora_fim === 0){
            $hora_fim = ' 00:59:59';
        } else {
            if (Yii::$app->user->identity->hora_inicio != NULL){
                $hora_fim = ' '.Yii::$app->user->identity->hora_inicio.':59:59';
            } else if (Yii::$app->user->identity->hora_inicio === 0){
                $hora_fim = ' 00:59:59';
            } else {
                $hora_fim = ' 23:59:59';
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_total = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->all());
            } else {
                $contageligacoes_total = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->all());
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_total = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->all());
            } else {
                $contageligacoes_total = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->all());
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_atendidas = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all());
            } else {
                $contageligacoes_atendidas = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all());
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_atendidas = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all());
            } else {
                $contageligacoes_atendidas = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->all());
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_ocupado = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all());
            } else {
                $contageligacoes_ocupado = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all());
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $contageligacoes_ocupado = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all());
            } else {
                $contageligacoes_ocupado = count(Voip::find()->select(['calldate','src'])->distinct()
                        ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['!=', 'dst', 's'])
                        ->andWhere(['=', 'disposition', 'BUSY'])
                        ->all());
            }
        }
        
        $contageligacoes_abandonatas = $contageligacoes_total - $contageligacoes_atendidas;
        
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $maxima_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->max('atraso');
            } else {
                $maxima_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->max('atraso');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $maxima_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->max('atraso');
            } else {
                $maxima_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->max('atraso');
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $media_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->average('atraso');
            } else {
                $media_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->average('atraso');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $media_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->average('atraso');
            } else {
                $media_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->average('atraso');
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $minima_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['>', 'atraso', 0])
                        ->min('atraso');
            } else {
                $minima_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['>', 'atraso', 0])
                        ->min('atraso');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $minima_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['>', 'atraso', 0])
                        ->min('atraso');
            } else {
                $minima_atraso = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['>', 'atraso', 0])
                        ->min('atraso');
            }
        }
        
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $maxima_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->max('billsec');
            } else {
                $maxima_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->max('billsec');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $maxima_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->max('billsec');
            } else {
                $maxima_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->max('billsec');
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $media_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->average('billsec');
            } else {
                $media_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->average('billsec');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $media_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->average('billsec');
            } else {
                $media_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->average('billsec');
            }
        }
        if (Yii::$app->user->identity->voip_origem) {
            if (Yii::$app->user->identity->voip_destino) {
                $minima_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->andWhere(['>', 'billsec', 0])
                        ->min('billsec');
            } else {
                $minima_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->andWhere(['>', 'billsec', 0])
                        ->min('billsec');
            }
        } else {
            if (Yii::$app->user->identity->voip_destino) {
                $minima_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->andWhere(['>', 'billsec', 0])
                        ->min('billsec');
            } else {
                $minima_conversa = Voip::find()
                        ->andWhere(['=', 'usuario_fk', Yii::$app->user->identity->id])
                        ->andWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk])
                        ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                        ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                        ->andWhere(['=', 'disposition', 'ANSWERED'])
                        ->andWhere(['>', 'billsec', 0])
                        ->min('billsec');
            }
        }
        
        return $this->render('index', [
            'model' => $searchModel,
            'modelusuario' => $ModelUsuario,
            'dataProvider' => $dataProvider,
            'contageligacoes_total' => $contageligacoes_total,
            'contageligacoes_atendidas' => $contageligacoes_atendidas,
            'contageligacoes_abandonatas' => $contageligacoes_abandonatas,
            'contageligacoes_ocupado' => $contageligacoes_ocupado,
            'maxima_atraso' => $maxima_atraso,
            'media_atraso' => $media_atraso,
            'minima_atraso' => $minima_atraso,
            'maxima_conversa' => $maxima_conversa,
            'media_conversa' => $media_conversa,
            'minima_conversa' => $minima_conversa,
        ]);
    }
    
    protected function findModel($id) {
        if (($model = Voip::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('A página solicitada não existe.');
        }
    }
    
    public function actionSetSessionData() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->data_asterisk = $request['data_asterisk'];
            $model->save();
        }
        Yii::$app->user->identity->data_asterisk = $request['data_asterisk'];
    }
    
    public function actionSetSessionVoiporigem() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->voip_origem = $request['voip_origem'];
            $model->save();
        }
        Yii::$app->user->identity->voip_origem = $request['voip_origem'];
    }
    
    public function actionSetSessionVoipdestino() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->voip_destino = $request['voip_destino'];
            $model->save();
        }
        Yii::$app->user->identity->voip_destino = $request['voip_destino'];
    }
    
    public function actionSetSessionHorainicio() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->hora_inicio = $request['hora_inicio'];
            $model->save();
        }
        Yii::$app->user->identity->hora_inicio = $request['hora_inicio'];
    }
    
    public function actionSetSessionHorafim() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->hora_fim = $request['hora_fim'];
            $model->save();
        }
        Yii::$app->user->identity->hora_fim = $request['hora_fim'];
    }
    
    public function actionSetSessionStatuschamada() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->status_chamada = $request['status_chamada'];
            $model->save();
        }
        Yii::$app->user->identity->status_chamada = $request['status_chamada'];
    }
    
    public function actionClean() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        if ($model) {
            $model->voip_origem = NULL;
            $model->voip_destino = NULL;
            $model->hora_inicio = NULL;
            $model->hora_fim = NULL;
            $model->status_chamada = NULL;
            $model->save();
        }
        return $this->redirect(['offline/index']);
    }

    public function actionFiltro() {
        $model_fila_delela = Voip::find()
                ->andwhere(['usuario_fk' => Yii::$app->user->identity->id])
                ->andwhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk])
                ->all();

        foreach($model_fila_delela as $array_fila_delela){
            $this->findModel($array_fila_delela->id)->delete();
        }
    
        if (Yii::$app->user->identity->hora_inicio != NULL){
            $hora_inicio = ' '.Yii::$app->user->identity->hora_inicio.':00:00';
        } else if (Yii::$app->user->identity->hora_inicio === 0){
        $hora_inicio = ' 00:00:00';
        } else {
            $hora_inicio = ' 00:00:00';
        }
        if (Yii::$app->user->identity->hora_fim != NULL){
            $hora_fim = ' '.Yii::$app->user->identity->hora_fim.':59:59';
        } else if (Yii::$app->user->identity->hora_fim === 0){
            $hora_fim = ' 00:59:59';
        } else {
            if (Yii::$app->user->identity->hora_inicio != NULL){
                $hora_fim = ' '.Yii::$app->user->identity->hora_inicio.':59:59';
            } else if (Yii::$app->user->identity->hora_inicio === 0){
                $hora_fim = ' 00:59:59';
            } else {
        $hora_fim = ' 23:59:59';
            }
        }
        
        $busca_voip = Cdr::find()
            ->Where(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
            ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
            ->andWhere(['!=', 'dst', 's'])
            ->orderBy(['calldate' => SORT_ASC])
            ->all();
        foreach ($busca_voip as $key_voip => $value_voip) {
            
            $atraso = $value_voip->duration - $value_voip->billsec;

            $model = new Voip();

            $model->calldate = $value_voip->calldate;
            $model->src = $value_voip->src;
            $model->dst = $value_voip->dst;
            $model->dcontext = $value_voip->dcontext;
            $model->atraso = $atraso;
            $model->billsec = $value_voip->billsec;
            $model->duration = $value_voip->duration;
            $model->disposition = $value_voip->disposition;
            $model->usuario_fk = Yii::$app->user->identity->id;
            $model->unidade_fk = Yii::$app->user->identity->unidade_temp_fk;
            $model->save();
        }
        
        return $this->redirect(['offline/index']);
    }
    
    public function actionExport() {
        $searchModel = new Voip();
        
        $export = new Spreadsheet([
            'dataProvider' => $searchModel->search(Yii::$app->request->queryParams),
            'columns' => [
                'calldate',
                'src',
                'dst',
                [
                    'attribute' => 'atraso',
                    'value' => function ($data) {
                        return gmdate("H:i:s", $data->atraso);
                    },
                ],
                [
                    'attribute' => 'billsec',
                    'value' => function ($data) {
                        return gmdate("H:i:s", $data->billsec);
                    },
                ],
                [
                    'attribute' => 'duration',
                    'value' => function ($data) {
                        return gmdate("H:i:s", $data->duration);
                    },
                ],
                [
                    'attribute' => 'disposition',
                    'value' => function ($data) {
                        switch ($data->disposition) {
                            case 'NO ANSWER':
                                $status = 'Não atendida';
                                break;
                            case 'ANSWERED':
                                $status = 'Atendida';
                                break;
                            case 'BUSY':
                                $status = 'Ocupado';
                                break;
                            default:
                                $status = $data->disposition;
                        }
                        return $status;
                    },
                ],
            ],
        ]);
        return $export->send('Offline.xls');
    }
    
}
