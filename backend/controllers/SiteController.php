<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Usuario;
use common\models\Sistema;
use common\components\Setup;
use common\models\UsuarioPerfil;
use common\components\SendMail;
use common\models\Computador;
use common\models\Monitor;
use common\models\Equipamento;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'new-password', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'set-session-unidade', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->session->isActive) {
            
            $url = Sistema::findOne(1);
            Yii::$app->session->set("url", $url->url);

        }
        

        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        $this->layout = "main_login";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    public function actionSetSessionUnidade() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $request = Yii::$app->request->post();
        if ($model) {
            $model->unidade_temp_fk = $request['unidade_temp_fk'];
            $model->save();
        }
        Yii::$app->user->identity->unidade_temp_fk = $request['unidade_temp_fk'];
    }
    
    public function actionResetPassword() {
        $this->layout = "main_login";
        $model = new Usuario();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model = Usuario::find()->where(['email' => $model->email])->all();
                foreach ($model as $key => $value) {
                    $value->token = Yii::$app->security->generateRandomString();
                    if ($value->save()) {
                        Yii::$app->mailer->compose([
                                    'html' => 'novo_cadastro_insignare-html',
                                    'text' => 'novo_cadastro_insignare-text'
                                        ], [
                                    'model' => $value
                                ])->setFrom([
                                    'insignare@acpr.org.br' => 'Insignare'
                                ])->setTo($value->email)
                                ->setSubject('Atualização de senha!')
                                ->send();
                    } else {
                        throw new \Exception(Setup::setMessageErrorModel('Atenção:', $model->getErrors()));
                    }
                }
                Yii::$app->session->setFlash('success', 'Registro atualizado com sucesso!');
                return $this->redirect(['site/login']);
            } else {
                $model->senha = '';
            }
        }
        return $this->render('reset_password', [
                    'model' => $model,
        ]);
    }

    public function actionNewPassword($token) {
        $this->layout = "main_login";
        $model = Usuario::findOne(['token' => $token]);

        if ($model) {
            if (Yii::$app->request->isAjax) {
                if ($model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
            } else {
                if ($model->load(Yii::$app->request->post())) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $model->senha = Yii::$app->security->generatePasswordHash($model->senha);
                        $model->senha_hash = $model->senha;
                        $model->auth_key = Yii::$app->security->generateRandomString();
                        $model->token = Yii::$app->security->generateRandomString();
                        if ($model->save()) {
                            $transaction->commit();
                            $emails = [ $model->email ];
                            SendMail::submitpassword($emails, $model, 'email_usuario_senha_atualizacao-html');
                            return $this->redirect(['site/login']);
                        } else {
                            throw new \Exception(Setup::setMessageErrorModel('Atenção:', $model->getErrors()));
                        }
                    } catch (\Exception $e) {
                        $mensagem = $e->getMessage();
                        $transaction->rollBack();
                        $model->load(Yii::$app->request->post());
                        Yii::$app->session->setFlash('error', $mensagem);
                    }
                } else {
                    $model->senha = '';
                }
            }
            return $this->render('new_password', [
                'model' => $model,
            ]);
        } else {
            return $this->goHome();
        }
    }
    
    public function actionProfile() {
        $model = Usuario::findOne(Yii::$app->user->identity->id);
        $model->senha = '';
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                $model_anterior = Usuario::findOne(Yii::$app->user->identity->id);
                if ($model_anterior->senha != $model->senha){
                    $model->senha = Yii::$app->security->generatePasswordHash($model->senha);
                    $model->senha_hash = $model->senha;
                }
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect(['site/profile']);
                }
            }
        }
        return $this->render('perfil', [
                    'title' => 'Editar meu Usuario',
                    'model' => $model,
        ]);
    }
    
}
