<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css',
        'plugins/switchery/css/switchery.min.css',
        'plugins/multiselect/css/multi-select.css',
        'plugins/select2/css/select2.css',
        'plugins/bootstrap-select/css/bootstrap-select.min.css',
        'plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css',
        'plugins/custombox/css/custombox.css',
        'plugins/jquery-loading/jquery.loading.min.css',
        'plugins/bootstrap-sweetalert/sweet-alert.css',
        'plugins/jstree/style.css',
        'css/core.css',
        'css/components.css',
        'css/icons.css',
        'css/pages.css',
        'css/responsive.css',
        'css/variables.css'
    ];
    public $js = [
        'js/modernizr.min.js',
        'js/detect.js',
        'js/fastclick.js',
        'js/jquery.slimscroll.js',
        'js/jquery.blockUI.js',
        'js/waves.js',
        'js/wow.min.js',
        'js/jquery.nicescroll.js',
        'js/jquery.scrollTo.min.js',
        'plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js',
        'plugins/switchery/js/switchery.min.js',
        'plugins/multiselect/js/jquery.multi-select.js',
        'plugins/jquery-quicksearch/jquery.quicksearch.js',
        'plugins/select2/js/select2.min.js',
        'plugins/bootstrap-select/js/bootstrap-select.min.js',
        'plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js',
        'plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js',
        'plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
        'plugins/peity/jquery.peity.min.js',
        'plugins/waypoints/lib/jquery.waypoints.js',
        'plugins/counterup/jquery.counterup.min.js',
        'plugins/raphael/raphael-min.js',
        'plugins/jquery-loading/jquery.loading.min.js',
        'plugins/bootstrap-sweetalert/sweet-alert.min.js',
        'js/jquery.core.js',
        'js/jquery.app.js',
        'plugins/custombox/js/custombox.min.js',
        'plugins/custombox/js/legacy.min.js',
        'plugins/jstree/jstree.min.js',
        'pages/jquery.tree.js',
        'js/bootbox.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];

}
