<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\grid\ActionColumn;

class SYSPerfilActionColumn extends ActionColumn {

    public $headerOptions = ['class' => 'actions-buttons text-center'];
    public $contentOptions = ['class' => 'text-center'];
    public $width = '130px';
    public $viewButtonVisible = true;
    public $updateButtonVisible = true;
    public $deleteButtonVisible = true;
    public $menuButtonVisible = true;
    public $restricaoButtonVisible = true;

    function init() {
        parent::init();
        $this->template = "<span>{view} {update} {delete} {menu} {restrict}</span>";
        $this->initDefaultButtons();
    }

    public function run() {
        return Html::decode($this->contentOptions);
    }

    protected function initDefaultButtons() {
        if (($this->viewButtonVisible) && (!isset($this->buttons['view']))) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'View'),
                    'aria-label' => Yii::t('app', 'View'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-default loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
            };
        }
        if (($this->updateButtonVisible) && (!isset($this->buttons['update']))) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Edit'),
                    'aria-label' => Yii::t('app', 'Edit'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-primary loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (($this->deleteButtonVisible) && (!isset($this->buttons['delete']))) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Delete'),
                    'aria-label' => Yii::t('app', 'Delete'),
                    'data-confirm' => Yii::t('app', 'Tem certeza que deseja excluir este item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-danger',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
        if (($this->menuButtonVisible) && (!isset($this->buttons['menu']))) {
            $this->buttons['menu'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Menu'),
                    'aria-label' => Yii::t('app', 'Menu'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-warning loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-tasks"></span>', $url, $options);
            };
        }
        if (($this->restricaoButtonVisible) && (!isset($this->buttons['restrict']))) {
            $this->buttons['restrict'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Restrição'),
                    'aria-label' => Yii::t('app', 'Restrição'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-sm btn-purple loading',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, $options);
            };
        }
    }

}
