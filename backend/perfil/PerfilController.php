<?php

namespace backend\controllers;

use Yii;
use common\models\Perfil;
use common\models\Menu;
use common\models\MenuPerfil;
use common\models\RestricaoMenuPerfil;
use common\models\Sistema;
use common\components\AccessRulesControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\IntegrityException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\helpers\Json;
use yii\helpers\Html;

class PerfilController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete', 'menu', 'restrict'],
                'rules' => AccessRulesControl::getRulesControl('perfil'),
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Perfil();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('urlPerfilSearch', Yii::$app->request->url);

        return $this->render('index', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'title' => 'Detalhes do Perfil',
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Perfil();

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro inserido com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlPerfilSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Adicionar novo Perfil',
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        } else {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Registro alterado com sucesso!');
                    return $this->redirect([Yii::$app->session->get('urlPerfilSearch', array())]);
                }
            }
        }
        return $this->render('_form', [
                    'title' => 'Alterar Perfil',
                    'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        try {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', 'Registro removido com sucesso!');
        } catch (IntegrityException $e) {
            if ($e->getCode() == 23503) {
                $mensagem = 'Existem tabelas relacionadas a este cadastro, não é possivel excluir!';
            } else {
                $mensagem = $e->getMessage() . ' ' . $e->getTraceAsString();
            }
            Yii::$app->session->setFlash('error', $mensagem);
        }
        return $this->redirect([Yii::$app->session->get('urlPerfilSearch', array())]);
    }

    protected function findModel($id) {
        if (($model = Perfil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRestrict($id) {
        $menu = Menu::find()->alias('menu')->join('JOIN', 'tb_menu_perfil', 'tb_menu_perfil.menu_fk = menu.id')->where(['tb_menu_perfil.perfil_fk' => $id, 'nivel' => '1'])->all();

        return $this->render('restrict', [
                    'title' => 'Ajustar Restrição do Perfil',
                    'model' => Perfil::findOne($id),
                    'menu' => $menu,
        ]);
    }

    public function actionRestricoes() {
        $request = Yii::$app->request->post();
        $menu = Menu::findOne($request['menu']);
        $string_controller = (String) 'backend\controllers\\' . trim(str_replace(" ", "", ucwords(str_replace("-", " ", $menu->controller))) . 'Controller');
        $controller = $string_controller::behaviors();
        $actions = $controller['access']['only'];
        $html = '';
        foreach ($actions as $key => $value) {
            $restricao = RestricaoMenuPerfil::find()->where(['menu_fk' => $menu->id, 'perfil_fk' => $request['perfil'], 'restricao' => $value])->one();
            $html .= '<div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">' 
                    . Html::checkbox('restricoes[' . $value . ']', ($restricao ? true : false), ['label' => $value, 'class' => 'switchery switchery-default', 'data-plugin' => "switchery", 'data-switchery' => "true"])
                    . '     </div>
                        </div>
                      </div>';
        }
        $return['html'] = $html;
        return Json::encode($return);
    }

    public function actionMenu($id) {
        $data = '';
        $model_sistema = Sistema::find()->orderBy(['id' => SORT_ASC])->all();
        foreach ($model_sistema as $key => $value) {
            $model_menu_sistema = Menu::find()->where(['sistema_fk' => $value->id, 'nivel' => '0'])->orderBy(['id' => SORT_ASC])->all();
            if ($model_menu_sistema) {
                $data .= '<li data-jstree=\'{"opened":true}\'>' . $value->nome;
                $data .= '<ul>';
                foreach ($model_menu_sistema as $keySistema => $valueSistema) {
                    $model_perfil = MenuPerfil::find()->where(['menu_fk' => $valueSistema->id, 'perfil_fk' => $id])->one();
                    $model_menu_primeiro_nivel = Menu::find()->where(['sistema_fk' => $value->id, 'menu_pai_fk' => $valueSistema->id])->orderBy(['id' => SORT_ASC])->all();
                    if ($model_menu_primeiro_nivel) {
                        $data .= '<li data-jstree=\'{"opened":true, "icon":"' . $valueSistema->icon . '"}\'>' . $valueSistema->titulo;
                        $data .= '<ul>';
                        foreach ($model_menu_primeiro_nivel as $keyPrimeiro => $valuePrimeiro) {
                            $model_perfil = MenuPerfil::find()->where(['menu_fk' => $valuePrimeiro->id, 'perfil_fk' => $id])->one();
                            $data .= '<li data-jstree=\'{"tipo": "menu", "icon":"' . $valuePrimeiro->icon . '", "key": "' . $valuePrimeiro->id . '", ' . (!empty($model_perfil) ? '"selected":true' : '"selected":false') . '}\'>' . $valuePrimeiro->titulo . '</li>';
                        }
                        $data .= '</ul>';
                        $data .= '</li>';
                    } else {
                        $data .= '<li data-jstree=\'{"tipo": "menu", "icon":"' . $valueSistema->icon . '", "key": "' . $valueSistema->id . '"}\'>' . $valueSistema->titulo . '</li>';
                    }
                }
                $data .= '</ul>';
                $data .= '</li>';
            } else {
                $data .= '<li data-jstree=\'{"opened":true\'>' . $value->nome;
            }
        }

        return $this->render('menu', [
                    'title' => 'Associação menu ao Perfil',
                    'data' => $data,
                    'model' => Perfil::findOne($id),
        ]);
    }

    public function actionSaveMenuPerfil() {
        $request = Yii::$app->request->post();
        $array = json_decode($request['dados']);
        MenuPerfil::deleteAll(['perfil_fk' => $request['id']]);
        foreach ($array as $key => $value) {
            if ($value->selected == 'true') {
                $menu = Menu::findOne($value->key);
                $menu_perfil = MenuPerfil::findOne(['perfil_fk' => $request['id'], 'menu_fk' => $menu->menu_pai_fk]);
                if (!$menu_perfil) {
                    $perfil = new MenuPerfil();
                    $perfil->perfil_fk = $request['id'];
                    $perfil->menu_fk = $menu->menu_pai_fk;
                    $perfil->save();
                }
                $menu_perfil = MenuPerfil::findOne(['perfil_fk' => $request['id'], 'menu_fk' => $value->key]);
                if (!$menu_perfil) {
                    $perfil = new MenuPerfil();
                    $perfil->perfil_fk = $request['id'];
                    $perfil->menu_fk = $value->key;
                    $perfil->save();
                }
            }
        }
        $results['status'] = 'success';
        return Json::encode($results);
    }
    
    public function actionSaveRestricaoMenuPerfil() {
        $request = Yii::$app->request->post();
        RestricaoMenuPerfil::deleteAll(['perfil_fk' => $request['Perfil']['id'], 'menu_fk' => $request['Perfil']['menu']]);
        if (isset($request['restricoes']) && $request['restricoes']) {
            foreach ($request['restricoes'] as $key => $value) {
                $restricao = new RestricaoMenuPerfil();
                $restricao->perfil_fk = $request['Perfil']['id'];
                $restricao->menu_fk = $request['Perfil']['menu'];
                $restricao->restricao = $key;
                $restricao->save();
            }
        }
        Yii::$app->session->setFlash('success', 'Restrições atualizadas com sucesso!');
        return $this->redirect([Yii::$app->session->get('urlPerfilSearch', array())]);
    }

}
