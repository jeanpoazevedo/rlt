<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Restição de menu';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'form-menu-perfil',
                            'action' => '/perfil/save-restricao-menu-perfil',
                            'method' => 'post',
                            'enableClientValidation' => true,
                ]);
                ?>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="text-dark header-title m-t-0 m-b-30">Restrição do sistema para o perfil: <?php echo $model->nome; ?></h4>

                            <?php echo $form->field($model, 'id')->hiddenInput()->label(false); ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    echo $form->field($model, 'menu')->widget(Select2::className(), [
                                        'data' => ArrayHelper::map($menu, 'id', 'titulo'),
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        'pluginOptions' => [
                                            'allowClear' => false
                                        ],
                                        'options' => [
                                            'prompt' => 'Selecione um Menu',
                                            'onchange' => "
                                                            if ($('#perfil-menu').val()) {
                                                                $.post('/perfil/restricoes', {menu: $('#perfil-menu').val(), perfil: $('#perfil-id').val()}, function (data) {
                                                                    $('#restricao').html(data.html);
                                                                }, 'json');
                                                            }
                                                        ",
                                        ],
                                    ]);
                                    ?>
                                </div> 
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <span id="restricao"></span>
                                </div> 
                            </div>
                        </div>
                    </div>            
                </div>            

                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlPerfilSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton(Yii::t('app', '<i class="ion-checkmark-round"></i> Ajustar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>