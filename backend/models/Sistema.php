<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_sistema".
 *
 * @property int $id
 * @property string $nome
 * @property string $url
 * @property string $rodape_login
 * @property string $rodape_interno
 *
 * @property TbMenu[] $tbMenus
 */
class Sistema extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_sistema';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'url'], 'required'],
            [['nome', 'url'], 'string', 'max' => 1024],
            [['rodape_login', 'rodape_interno'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'url' => 'Url',
            'rodape_login' => 'Rodape Login',
            'rodape_interno' => 'Rodape Interno',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbMenus()
    {
        return $this->hasMany(TbMenu::className(), ['sistema_fk' => 'id']);
    }
}
