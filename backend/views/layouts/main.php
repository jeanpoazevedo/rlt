<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use common\widgets\Alert;

AppAsset::register($this);
$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="fixed-left">
        <?php $this->beginBody() ?>

        <div id="wrapper">
            <div class="topbar">

                <div class="topbar-left">
                    <div class="text-center">
                        <a href="/site/index" class="logo">
                            <i class="icon-c-logo"> <img src="/img/logo.png" height="42"/> </i>
                            <span><img src="/img/logo.png" width="50px"></span>
                        </a>
                    </div>
                </div>

                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button id="button_close_menu" class="button-menu-mobile open-left">
                                    <i class="ion-navicon"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav navbar-right pull-left">
                                <?php echo $this->render('_topbar') ?>
                            </ul>
                            <ul class="nav navbar-nav navbar-right pull-right">
                                <?php
                                    echo $this->render('_unidade');
                                ?>
                                <li class="dropdown hidden-xs">
                                    <br>
                                    Seja bem vindo(a). <?php echo Yii::$app->user->identity->nome; ?>.
                                </li>                                
                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                        <img src="/img/user.png" alt="user-img" class="img-circle">
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="/site/profile/<?php echo Yii::$app->user->identity->id; ?>"><i class="ti-user m-r-10 text-custom"></i>Perfil</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="/site/logout"><i class="ti-power-off m-r-10 text-danger"></i>Logout</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="hidden-xs">
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div id="sidebar-menu">
                        <?php echo $this->render('_sidebar') ?>
                    </div>
                </div>
            </div>

            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <?php echo Alert::widget() ?>
                        <?php echo $content ?>
                    </div>
                </div>
                <footer class="footer text-right">
                    RLT - 2019 Versão 2.0
                </footer>
            </div>
        </div>

        <?php $this->endBody() ?>
        <script>
            var resizefunc = [];
        </script>
    </body>
</html>
<?php $this->endPage() ?>
