<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use common\widgets\Alert;

AppAsset::register($this);
$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <h3 class="text-center"><img width="250px" src="/img/logo.png"></h3>
            <?php echo Alert::widget() ?>
            <?php echo $content ?>
        </div>

        <?php $this->endBody() ?>
        <script>
            var resizefunc = [];
        </script>
    </body>
</html>
<?php $this->endPage() ?>
