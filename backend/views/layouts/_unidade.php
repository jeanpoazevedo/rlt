<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\UsuarioUnidade;
?>

<div class="col-sm-12" style="width: 200px; padding: 10px;">
    <?php
    echo Select2::widget([
        'name' => 'unidade_temp_fk',
        'data' => ArrayHelper::map(UsuarioUnidade::find()->orWhere(['usuario_fk' => Yii::$app->user->identity->id])->all(), 'unidade_fk', function($model) {
                    return $model->unidadeFk->campoFk->campo . ' - ' . $model->unidadeFk->unidade;
                }),
        'value' => Yii::$app->user->identity->unidade_temp_fk,
        'pluginOptions' => [
            'width' => '300px',
        ],
        'options' => [
            'id' => 'unidade_temp_fk',
            'onchange' => '
                if ($("#unidade_temp_fk").val()) {
                    $.post("/site/set-session-unidade", {unidade_temp_fk: $("#unidade_temp_fk").val()}, function (data) {
                        location.reload(true);
                    });
                }
            ',
        ],
    ]);
    ?>
</div>