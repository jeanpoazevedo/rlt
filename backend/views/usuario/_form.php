<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use common\models\Unidade;
use common\models\Perfil;
use common\models\UsuarioUnidade;

$this->title = 'Usuario';

?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'nome')->textInput(['class' => 'form-control', 'maxlength' => true, 'autofocus' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'email')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?php echo $form->field($model, 'usuario')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            if ($usuarioperfil->perfil_fk == 1) {
                                echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Unidade::find()->all(), 'id', function($model) {
                                                return $model->campoFk->campo . ' - ' . $model->unidade;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else if ($usuarioperfil->perfil_fk == 2) {
                                echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(UsuarioUnidade::find()->orWhere(['usuario_fk' => Yii::$app->user->identity->id])->all(), 'unidade_fk', function($model) {
                                        return $model->unidadeFk->campoFk->campo . ' - ' . $model->unidadeFk->unidade;
                                    }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php
                            if ($usuarioperfil->perfil_fk == 1) {
                                echo $form->field($model_perfil, 'perfil_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Perfil::find()->orderBy(['nome' => SORT_DESC])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Perfil'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else if ($usuarioperfil->perfil_fk == 2) {
                                echo $form->field($model_perfil, 'perfil_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Perfil::find()->where(['>=', 'id', $usuarioperfil->perfil_fk])->orderBy(['nome' => SORT_DESC])->all(), 'id', function($model) {
                                                return $model->nome;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Perfil'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?php
                                echo $form->field($model, 'status', ['template' => '{label}<div class="input-group">{input}</div>',])
                                    ->checkbox(['data-plugin' => "switchery"]);
                            ?>
                        </div>                                        
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlUsuarioSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

<?php
    $this->registerJs(" 

            function verifica_forca(senha){
                forca = 0;
                mostra = document.getElementById('mostra');
                if((senha.length > 7) && (senha.length <= 12)){
                    forca += 10;
                }else if((senha.length>12)){
                    forca += 25;
                }
                if(senha.match(/[a-z]+/)){
                    forca += 10;
                }
                if(senha.match(/[A-Z]+/)){
                    forca += 20;
                }
                if(senha.match(/\d+/)){
                    forca += 20;
                }
                if(senha.match(/\W+/)){
                    forca += 25;
                }
                return mostra_res();
                return libera_aplicar();
            }
            function mostra_res(){
                if(forca <=30){
                    mostra.innerHTML = '<span style=\"color:#FF0000;\"> Fraca </span>';
                }else if((forca > 30) && (forca < 60)){
                    mostra.innerHTML = '<span style=\"color:#FFBE00;\"> Boa </span>'; 
                }else if((forca >= 60) && (forca < 85)){
                    mostra.innerHTML = '<span style=\"color:#0000FF;\"> Forte </span>';
                }else {
                    mostra.innerHTML = '<span style=\"color:#00CD00;\"> Excelente </span>';
                }
            }

       ", View::POS_END, 'functions-usuario-password'
    );
?>