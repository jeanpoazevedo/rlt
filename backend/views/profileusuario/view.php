<?php

use yii\helpers\Html;

$this->title = 'Usuario Perfil';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('usuario_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->usuarioFk->nome; ?>">
                        </div>                                        
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('perfil_fk'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->perfilFk->nome; ?>">
                        </div>                                        
                    </div>           
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlProfileusuarioSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                    <?php
                    echo Html::a('<i class="ion-close"></i> Excluir', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Deseja realmente excluir este item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>