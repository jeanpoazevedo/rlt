<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Faça uma nova senha!';
?>
<div class="row">
    <div class="col-xs-12">
    </div>
    <div class="col-xs-12">
        <div class="card-box">

            <div class="panel-heading">
                <h3 class="text-left">Troca minha senha</h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'form-horizontal m-t-20']); ?>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="control-label"><?php echo $model->getAttributeLabel('nome'); ?></label>
                        <input type="text" class="form-control" disabled="" value="<?php echo $model->nome; ?>">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="control-label"><?php echo $model->getAttributeLabel('email'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->email; ?>">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="control-label"><?php echo $model->getAttributeLabel('usuario'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->usuario; ?>">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <?php echo $form->field($model, 'senha')->passwordInput(['placeholder' => 'Sua Senha', 'class' => 'form-control'])->label(''); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo Html::submitButton('Atualizar', ['class' => 'btn btn-primary btn-block text-uppercase waves-effect waves-light', 'name' => 'login-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
    </div>
</div>