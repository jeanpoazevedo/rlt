<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class=" card-box">
    <div class="panel-heading"> 
        <h3 class="text-center"> <?php echo Html::encode($this->title) ?> </h3>
    </div> 

    <p>Por favor, preencha os seguintes campos para entrar:</p>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?php echo $form->field($model, 'username')->textInput(['autofocus' => true]); ?>

                <?php echo $form->field($model, 'password')->passwordInput(); ?>

                <div class="form-group">
                    <?php echo Html::submitButton('Entrar', ['class' => 'btn btn-primary', 'name' => 'login-button']); ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
