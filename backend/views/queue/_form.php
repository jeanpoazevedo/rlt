<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;
use common\models\Unidade;
use common\models\Perfil;
use common\models\UsuarioUnidade;

$this->title = 'Fila';

?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-sm-3">
                        <?php
                            if ($usuarioperfil->perfil_fk == 1) {
                                echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(Unidade::find()->all(), 'id', function($model) {
                                                return $model->campoFk->campo . ' - ' . $model->unidade;
                                            }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            } else if ($usuarioperfil->perfil_fk == 2) {
                                echo $form->field($model, 'unidade_fk')->widget(Select2::className(), [
                                    'data' => ArrayHelper::map(UsuarioUnidade::find()->orWhere(['usuario_fk' => Yii::$app->user->identity->id])->all(), 'unidade_fk', function($model) {
                                        return $model->unidadeFk->campoFk->campo . ' - ' . $model->unidadeFk->unidade;
                                    }),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => [
                                        'prompt' => 'Selecione uma Unidade'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                ]);
                            }
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $form->field($model, 'queue')->textInput(['class' => 'form-control', 'maxlength' => true]) ?>
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlQueueSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ion-checkmark-round"></i> Adicionar') : Yii::t('app', '<i class="ion-checkmark-round"></i> Alterar'), ['class' => 'btn btn-primary waves-effect waves-light']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>