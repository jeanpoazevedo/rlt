<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Controle de cadastro de Perfil';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h3 class="panel-title">Lista de Perfil</h3>
            </div>
            <div class="panel-body panel-border-fix-inverse">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="m-b-30">
                            <?php echo Html::a(Yii::t('app', '<i class="ion-plus-round"></i> Novo Perfil'), ['create'], ['class' => 'btn btn-success loading']) ?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="m-b-30">
                            <?php
                            $form = ActiveForm::begin([
                                        'method' => 'get',
                            ]);
                            echo $form->field($model, 'pesquisa', [
                                'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                            ])->textInput(['placeholder' => "Pesquisar"]);
                            ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>

                <?php
                Pjax::begin([
                    'id' => 'perfil',
                    'enablePushState' => false,
                    'enableReplaceState' => false,
                ]);
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                    'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                    'columns' => [
                        'nome',
                        'descricao',
                        ['class' => 'common\components\SYSPerfilActionColumn'],
                    ],
                    'pager' => [
                        'options' => ['class' => 'pagination'],
                        'prevPageLabel' => 'Anterior',
                        'nextPageLabel' => 'Próximo',
                        'firstPageLabel' => 'Primeiro',
                        'lastPageLabel' => 'Último',
                        'nextPageCssClass' => 'next',
                        'prevPageCssClass' => 'prev',
                        'firstPageCssClass' => 'first',
                        'lastPageCssClass' => 'last',
                        'maxButtonCount' => 10,
                    ],
                ]);
                Pjax::end();
                ?>
            </div>
        </div>
    </div>
</div>
