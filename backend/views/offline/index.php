<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput;
use common\models\Cdr;
use common\models\Voip;
use common\models\Usuario;
use common\models\Queue;
use common\models\QueueAgents;

$this->title = 'Ligações Off-Line';

if (Yii::$app->user->identity->hora_inicio != NULL){
    $hora_inicio = ' '.Yii::$app->user->identity->hora_inicio.':00:00';
} else if (Yii::$app->user->identity->hora_inicio === 0){
    $hora_inicio = ' 00:00:00';
} else {
    $hora_inicio = ' 00:00:00';
}
if (Yii::$app->user->identity->hora_fim != NULL){
    $hora_fim = ' '.Yii::$app->user->identity->hora_fim.':59:59';
} else if (Yii::$app->user->identity->hora_fim === 0){
    $hora_fim = ' 00:59:59';
} else {
    if (Yii::$app->user->identity->hora_inicio != NULL){
        $hora_fim = ' '.Yii::$app->user->identity->hora_inicio.':59:59';
    } else if (Yii::$app->user->identity->hora_inicio === 0){
        $hora_fim = ' 00:59:59';
    } else {
        $hora_fim = ' 23:59:59';
    }
}

?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Ligações Off-Line</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php
                                    echo Html::a(Yii::t('app', 'Exportar'), ['export'], ['class' => 'btn btn-success']);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php
                                $form = ActiveForm::begin([
                                            'method' => 'get',
                                ]);
                                echo $form->field($model, 'pesquisa', [
                                    'template' => '<div class="form-group contact-search m-b-30">{input}<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button></div>',
                                ])->textInput(['placeholder' => "Pesquisar"]);
                                ActiveForm::end();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-info pull-left">
                                    <i class="md md-phone-in-talk text-info"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo $contageligacoes_total;?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Total de ligações</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-success pull-left">
                                    <i class="md md-call text-success"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo $contageligacoes_atendidas;?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Ligações atendidas</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-pink pull-left">
                                    <i class="md md-call-end text-pink"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo $contageligacoes_abandonatas;?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Ligações não atendidas</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-pink pull-left">
                                    <i class="md md-call-end text-pink"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo $contageligacoes_ocupado;?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Ocupado</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-info pull-left">
                                    <i class="md md-ring-volume text-info"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo gmdate("H:i:s", $maxima_atraso);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Máxima espera</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-info pull-left">
                                    <i class="md md-ring-volume text-info"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo gmdate("H:i:s", $media_atraso);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Média espera</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-info pull-left">
                                    <i class="md md-ring-volume text-info"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo gmdate("H:i:s", $minima_atraso);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Mínima espera</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-info pull-left">
                                    <i class="md md-call text-info"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo gmdate("H:i:s", $maxima_conversa);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Máxima Converssa</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-info pull-left">
                                    <i class="md md-call text-info"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo gmdate("H:i:s", $media_conversa);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Média Converssa</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="widget-bg-color-icon card-box fadeInDown animated">
                                <div class="bg-icon bg-icon-info pull-left">
                                    <i class="md md-call text-info"></i>
                                </div>
                                <div class="text-right">
                                    <h3 class="text-dark">
                                        <b class="counter">
                                            <?php echo gmdate("H:i:s", $minima_conversa);?>
                                        </b>
                                    </h3>
                                    <p class="text-muted">Mínima Converssa</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <?php
                                echo '<label class="control-label">Filtra: Data</label>';
                                echo DatePicker::widget([
                                    'name' => 'data_asterisk',
                                    'language' => 'pt-BR',
                                    'value' => $modelusuario->data_asterisk,
                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'orientation' => 'bottom left',
                                        'format' => 'dd/mm/yyyy',
                                        
                                    ],
                                    'options' => [
                                        'id' => 'data_asterisk',
                                        'onchange' => '
                                            $.post("/fila/set-session-data", {data_asterisk: $("#data_asterisk").val()}, function (data) {
                                                location.reload(true);
                                            });
                                        ',
                                    ]
                                ]);
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Número Origem</label>
                                <?php
                                    if (Yii::$app->user->identity->voip_destino) {
                                        $dataModel = ArrayHelper::map(Voip::find()->distinct()
                                                ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                                                ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                                                ->andWhere(['=', 'dst', Yii::$app->user->identity->voip_destino])
                                                ->andWhere(['!=', 'dst', 's'])
                                                ->orderBy(['src' => SORT_ASC])
                                                ->all(), 'src', function($model) {
                                            return $model->src;
                                        });
                                    } else {
                                        $dataModel = ArrayHelper::map(Voip::find()->distinct()
                                                ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                                                ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                                                ->andWhere(['!=', 'dst', 's'])
                                                ->orderBy(['src' => SORT_ASC])
                                                ->all(), 'src', function($model) {
                                            return $model->src;
                                        });
                                    }
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'voip_origem',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => $modelusuario->voip_origem,
                                        'options' => [
                                            'id' => 'voip_origem',
                                            'onchange' => '
                                                $.post("/fila/set-session-voiporigem", {voip_origem: $("#voip_origem").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Número Destino</label>
                                <?php
                                    if (Yii::$app->user->identity->voip_origem) {
                                        $dataModel = ArrayHelper::map(Voip::find()->distinct()
                                                ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                                                ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                                                ->andWhere(['=', 'src', Yii::$app->user->identity->voip_origem])
                                                ->andWhere(['!=', 'dst', 's'])
                                                ->orderBy(['dst' => SORT_ASC])
                                                ->all(), 'dst', function($model) {
                                            return $model->dst;
                                        });
                                    } else {
                                        $dataModel = ArrayHelper::map(Voip::find()->distinct()
                                                ->andWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio])
                                                ->andWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim])
                                                ->andWhere(['!=', 'dst', 's'])
                                                ->orderBy(['dst' => SORT_ASC])
                                                ->all(), 'dst', function($model) {
                                            return $model->dst;
                                        });
                                    }
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'voip_destino',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => $modelusuario->voip_destino,
                                        'options' => [
                                            'id' => 'voip_destino',
                                            'onchange' => '
                                                $.post("/fila/set-session-voipdestino", {voip_destino: $("#voip_destino").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Status</label>
                                <?php
                                    $modelstatus = new Usuario();
                                    $dataModel = ArrayHelper::map($modelstatus->array_status, 'disposition', function ($model) {
                                            return $model['status'];
                                        });
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'status_chamada',
                                        'data' => ArrayHelper::merge($dataArray, $dataModel),
                                        'value' => $modelusuario->status_chamada,
                                        'options' => [
                                            'id' => 'status_chamada',
                                            'onchange' => '
                                                $.post("/fila/set-session-statuschamada", {status_chamada: $("#status_chamada").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Hora Início</label>
                                <?php
                                    $modelhora = new Usuario();
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'hora_inicio',
                                        'data' => ArrayHelper::merge($dataArray, $modelhora->array_hora),
                                        'value' => $modelusuario->hora_inicio,
                                        'options' => [
                                            'id' => 'hora_inicio',
                                            'onchange' => '
                                                $.post("/fila/set-session-horainicio", {hora_inicio: $("#hora_inicio").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <label class="control-label">Filtra: Hora Fin</label>
                                <?php
                                    $modelhora = new Usuario();
                                    $dataArray = [ NULL => "Sem Filtro"];
                                    echo Select2::widget([
                                        'name' => 'hora_fim',
                                        'data' => ArrayHelper::merge($dataArray, $modelhora->array_hora),
                                        'value' => $modelusuario->hora_fim,
                                        'options' => [
                                            'id' => 'hora_fim',
                                            'onchange' => '
                                                $.post("/fila/set-session-horafim", {hora_fim: $("#hora_fim").val()}, function (data) {
                                                    location.reload(true);
                                                });
                                            ',
                                        ],
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', 'Limpa filtros'), ['clean'], ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="m-b-30">
                                <?php echo Html::a(Yii::t('app', 'Atualiza dados'), ['filtro'], ['class' => 'btn btn-success loading']) ?>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <?php
                        Pjax::begin([
                            'id' => 'ocorenciacomputadores',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                        ]);

                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{items}\n{summary}<div class='text-right'>{pager}</div>",
                            'summary' => "Exibindo {begin} - {end} de {totalCount} itens",
                            'columns' => [
                                'calldate',
                                'src',
                                'dst',
                                [
                                    'attribute' => 'atraso',
                                    'value' => function ($data) {
                                        return gmdate("H:i:s", $data->atraso);
                                    },
                                ],
                                [
                                    'attribute' => 'billsec',
                                    'value' => function ($data) {
                                        return gmdate("H:i:s", $data->billsec);
                                    },
                                ],
                                [
                                    'attribute' => 'duration',
                                    'value' => function ($data) {
                                        return gmdate("H:i:s", $data->duration);
                                    },
                                ],
                                [
                                    'attribute' => 'disposition',
                                    'value' => function ($data) {
                                        switch ($data->disposition) {
                                            case 'NO ANSWER':
                                                $status = 'Não atendida';
                                                break;
                                            case 'ANSWERED':
                                                $status = 'Atendida';
                                                break;
                                            case 'BUSY':
                                                $status = 'Ocupado';
                                                break;
                                            default:
                                                $status = $data->disposition;
                                        }
                                        return $status;
                                    },
                                ],
                            ],
                        ]);
                        Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>