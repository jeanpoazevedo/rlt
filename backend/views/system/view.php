<?php

use yii\helpers\Html;

$this->title = 'Sistema';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('nome'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->nome; ?>">
                        </div>                                        
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="control-label"><?php echo $model->getAttributeLabel('url'); ?></label>
                            <input type="text" class="form-control" disabled="" value="<?php echo $model->url; ?>">
                        </div>                                        
                    </div>
                </div>
                <div class="form-group text-right m-b-0">
                    <?php echo Html::a('<i class="ion-reply"></i> Voltar', [Yii::$app->session->get('urlSystemSearch', array())], ['class' => 'btn btn-default waves-effect waves-light m-l-5 loading']) ?>
                    <?php echo Html::a('<i class="ion-checkmark-round"></i> Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary loading']) ?>
                </div>

            </div>
        </div>
    </div>
</div>