<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'sourceLanguage' => 'pt-br',
    'language' => 'pt-br',
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'baseUrl' => '',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                '<controller:[0-9a-zA-Z\-]+>/<id:\d+>' => '<controller>/view',
                '<controller:[0-9a-zA-Z\-]+>/<action:[0-9a-zA-Z\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[0-9a-zA-Z\-]+>/<action:[0-9a-zA-Z\-]+>' => '<controller>/<action>',                
                '<module:[0-9a-zA-Z\-]+>/<controller:[0-9a-zA-Z\-]+>/<action:[0-9a-zA-Z\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:[0-9a-zA-Z\-]+>/<controller:[0-9a-zA-Z\-]+>/<action:[0-9a-zA-Z\-]+>'=>'<module>/<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];
