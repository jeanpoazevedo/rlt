<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_cdr_fila".
 *
 * @property int $id
 * @property int $usuario_fk
 * @property int $unidade_fk
 * @property string $calldate
 * @property string $src
 * @property string $dst
 * @property string $dcontext
 * @property int $atraso
 * @property int $duration
 * @property int $billsec
 * @property string $disposition
 *
 */
class Voip extends \yii\db\ActiveRecord
{
    
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_cdr_voip';
    }

    public function rules()
    {
        return [
            [['usuario_fk', 'unidade_fk', 'calldate'], 'required'],
            [['usuario_fk', 'unidade_fk', 'atraso', 'duration', 'billsec'], 'integer'],
            [['calldate'], 'safe'],
            [['src', 'dst', 'dcontext'], 'string', 'max' => 80],
            [['disposition'], 'string', 'max' => 45],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['usuario_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_fk' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_fk' => 'Usuario',
            'unidade_fk' => 'Unindade',
            'calldate' => 'Calldate',
            'src' => 'Origem',
            'dst' => 'Destino',
            'dcontext' => 'Tipo',
            'atraso' => 'Espera',
            'duration' => 'Duração Total',
            'billsec' => 'Tempo de Converssa',
            'disposition' => 'Status',
        ];
    }

    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getUsuarioFk()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_fk']);
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->calldate) {
            $this->calldate = Setup::convertApresentacao($this->calldate, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->calldate) {
            $this->calldate = Setup::convertBD($this->calldate, 'datetime');
        }
    }
    
    public function search($params) {
        $query = Voip::find()->select(['calldate','src', 'dst','atraso', 'duration','billsec', 'disposition'])->distinct();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'calldate' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Fila']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(src))', strtoupper(Setup::retirarAcento($params['Fila']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(dst))', strtoupper(Setup::retirarAcento($params['Fila']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(calldate))', strtoupper(Setup::retirarAcento($params['Fila']['pesquisa']))]);
        }
        
        if (Yii::$app->user->identity->hora_inicio != NULL){
            $hora_inicio = ' '.Yii::$app->user->identity->hora_inicio.':00:00';
        } else if (Yii::$app->user->identity->hora_inicio === 0){
            $hora_inicio = ' 00:00:00';
        } else {
            $hora_inicio = ' 00:00:00';
        }
        if (Yii::$app->user->identity->hora_fim != NULL){
            $hora_fim = ' '.Yii::$app->user->identity->hora_fim.':59:59';
        } else if (Yii::$app->user->identity->hora_fim === 0){
            $hora_fim = ' 00:59:59';
        } else {
            if (Yii::$app->user->identity->hora_inicio != NULL){
                $hora_fim = ' '.Yii::$app->user->identity->hora_inicio.':59:59';
            } else if (Yii::$app->user->identity->hora_inicio === 0){
                $hora_fim = ' 00:59:59';
            } else {
                $hora_fim = ' 23:59:59';
            }
        }
        $query->andFilterWhere(['=', 'usuario_fk', Yii::$app->user->identity->id]);
        $query->andFilterWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk]);
        $query->andFilterWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio]);
        $query->andFilterWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim]);
        
        if (Yii::$app->user->identity->voip_origem) {
            $query->andFilterWhere(['=', 'src', Yii::$app->user->identity->voip_origem]);
        }
        
        if (Yii::$app->user->identity->voip_destino) {
            $query->andFilterWhere(['=', 'dst', Yii::$app->user->identity->voip_destino]);
        }
        
        if (Yii::$app->user->identity->status_chamada) {
            $query->andFilterWhere(['=', 'disposition', Yii::$app->user->identity->status_chamada]);
        }
        
        $query->andFilterWhere(['!=', 'dst', 's']);
        
        return $dataProvider;
    }
}