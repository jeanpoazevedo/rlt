<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_sistema".
 *
 * @property int $id
 * @property string $nome
 * @property string $url
 *
 */
class Sistema extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_sistema';
    }
    
    public function rules()
    {
        return [
            [['nome', 'url'], 'required'],
            [['nome', 'url'], 'string', 'max' => 1024],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'url' => 'URL',
        ];
    }
    
    public function search($params) {
        $query = Sistema::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Sistema']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Sistema']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(url))', strtoupper(Setup::retirarAcento($params['Sistema']['pesquisa']))]);
        }

        return $dataProvider;
    }
}
