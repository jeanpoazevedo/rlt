<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "cdr".
 *
 * @property string $calldate
 * @property string $clid
 * @property string $src
 * @property string $dst
 * @property string $dcontext
 * @property string $channel
 * @property string $dstchannel
 * @property string $lastapp
 * @property string $lastdata
 * @property int $duration
 * @property int $billsec
 * @property string $disposition
 * @property int $amaflags
 * @property string $accountcode
 * @property string $uniqueid
 * @property string $userfield
 * @property string $did
 * @property string $recordingfile
 * @property string $cnum
 * @property string $cnam
 * @property string $outbound_cnum
 * @property string $outbound_cnam
 * @property string $dst_cnam
 * 
 */

class Cdr extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function getDb() {
        $bd = Unidade::findOne(Yii::$app->user->identity->unidade_temp_fk);
        return Yii::$app->get($bd->bd);
    }
    
    public static function tableName()
    {
        return 'cdr';
    }
    
    public function rules()
    {
        return [
            [['calldate'], 'safe'],
            [['duration', 'billsec', 'amaflags'], 'integer'],
            [['clid', 'src', 'dst', 'dcontext', 'channel', 'dstchannel', 'lastapp', 'lastdata'], 'string', 'max' => 80],
            [['disposition'], 'string', 'max' => 45],
            [['accountcode'], 'string', 'max' => 20],
            [['uniqueid'], 'string', 'max' => 32],
            [['userfield', 'recordingfile'], 'string', 'max' => 255],
            [['did'], 'string', 'max' => 50],
            [['cnum', 'cnam', 'outbound_cnum', 'outbound_cnam', 'dst_cnam'], 'string', 'max' => 40],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'calldate' => 'Data e Hora',
            'clid' => 'Clid',
            'src' => 'Origem',
            'dst' => 'Destino',
            'dcontext' => 'Dcontext',
            'channel' => 'Channel',
            'dstchannel' => 'Dstchannel',
            'lastapp' => 'Lastapp',
            'lastdata' => 'Lastdata',
            'duration' => 'Duração Total',
            'billsec' => 'Tempo de Converssa',
            'disposition' => 'Status',
            'amaflags' => 'Amaflags',
            'accountcode' => 'Accountcode',
            'uniqueid' => 'Uniqueid',
            'userfield' => 'Userfield',
            'did' => 'Did',
            'recordingfile' => 'Recordingfile',
            'cnum' => 'Cnum',
            'cnam' => 'Cnam',
            'outbound_cnum' => 'Outbound Cnum',
            'outbound_cnam' => 'Outbound Cnam',
            'dst_cnam' => 'Dst Cnam',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->calldate) {
            $this->calldate = Setup::convertApresentacao($this->calldate, 'datetime');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->calldate) {
            $this->calldate = Setup::convertBD($this->calldate, 'datetime');
        }
    }
    
    public function search($params) {
        $query = Cdr::find()->distinct();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'calldate' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Usuario']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(email))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))]);
        }
        
        if (Yii::$app->user->identity->hora_inicio != NULL){
            $hora_inicio = ' '.Yii::$app->user->identity->hora_inicio.':00:00';
        } else if (Yii::$app->user->identity->hora_inicio === 0){
            $hora_inicio = ' 00:00:00';
        } else {
            $hora_inicio = ' 00:00:00';
        }
        if (Yii::$app->user->identity->hora_fim != NULL){
            $hora_fim = ' '.Yii::$app->user->identity->hora_fim.':59:59';
        } else if (Yii::$app->user->identity->hora_fim === 0){
            $hora_fim = ' 00:59:59';
        } else {
            if (Yii::$app->user->identity->hora_inicio != NULL){
                $hora_fim = ' '.Yii::$app->user->identity->hora_inicio.':59:59';
            } else if (Yii::$app->user->identity->hora_inicio === 0){
                $hora_fim = ' 00:59:59';
            } else {
                $hora_fim = ' 23:59:59';
            }
        }
        
        $query->andFilterWhere(['>', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_inicio]);
        $query->andFilterWhere(['<', 'calldate', Yii::$app->user->identity->data_asterisk.$hora_fim]);
        
        if (Yii::$app->user->identity->voip_origem) {
            $query->andFilterWhere(['=', 'src', Yii::$app->user->identity->voip_origem]);
        }
        if (Yii::$app->user->identity->voip_destino) {
            $query->andFilterWhere(['=', 'dst', Yii::$app->user->identity->voip_destino]);
        }
        
        if (Yii::$app->user->identity->status_chamada) {
            $query->andFilterWhere(['=', 'disposition', Yii::$app->user->identity->status_chamada]);
        }
        
        $query->andFilterWhere(['!=', 'dst', 's']);
        
        return $dataProvider;
    }
    
}