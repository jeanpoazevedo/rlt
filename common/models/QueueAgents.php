<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "tb_queue_agents".
 *
 * @property int $id
 * @property int $queue_fk
 * @property int $agents
 *
 */
class QueueAgents extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_queue_agents';
    }
    
    public function rules()
    {
        return [
            [['queue_fk', 'agents'], 'required'],
            [['queue_fk', 'agents'], 'integer'],
            [['queue_fk', 'agents'], 'unique', 'targetAttribute' => ['queue_fk', 'agents']],
            [['queue_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'queue_fk' => 'Fila',
            'agents' => 'Membro',
        ];
    }
    
    public function getQueueFk()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_fk']);
    }
    
    public function search($params) {
        $query = QueueAgents::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'queue_fk' => SORT_ASC,
                    'agents' => SORT_ASC,
                ]
            ],
        ]);
        
        $query->alias('agents');
        $query->leftJoin('tb_queue', 'tb_queue.id = agents.queue_fk');
        if (isset($params['QueueAgents']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(agents))', strtoupper(Setup::retirarAcento($params['QueueAgents']['pesquisa']))]);
        }
        
        $query->andFilterWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk]);
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        if ($usuarioperfil->perfil_fk != '1') {
            $query->andFilterWhere(['tb_queue.unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);
        }
        
        return $dataProvider;
    }
    
}