<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "tb_queue".
 *
 * @property int $id
 * @property int $unidade_fk
 * @property int $queue
 *
 */
class Queue extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_queue';
    }
    
    public function rules()
    {
        return [
            [['unidade_fk', 'queue'], 'required'],
            [['unidade_fk', 'queue'], 'integer'],
            [['unidade_fk', 'queue'], 'unique', 'targetAttribute' => ['unidade_fk', 'queue']],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unidade_fk' => 'Unidade',
            'queue' => 'Fila',
        ];
    }
    
    public function getUnidadeFk()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function search($params) {
        $query = Queue::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'queue' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Queue']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(queue))', strtoupper(Setup::retirarAcento($params['Queue']['pesquisa']))]);
        }
        
        $query->andFilterWhere(['=', 'unidade_fk', Yii::$app->user->identity->unidade_temp_fk]);
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        if ($usuarioperfil->perfil_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);
        }
        
        return $dataProvider;
    }
    
}