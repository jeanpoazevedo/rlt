<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;
use common\components\FormatterCurrency;

/**
 * This is the model class for table "tb_usuario".
 *
 * @property int $id
 * @property string $usuario
 * @property string $senha
 * @property string $nome
 * @property string $email
 * @property int $unidade_fk
 * @property string $senha_hash
 * @property string $senha_reset_token
 * @property string $auth_key
 * @property string $token
 * @property int $status
 * @property string $data_asterisk
 * @property string $data_rlt
 * @property int $voip_origem
 * @property int $voip_destino
 * @property int $hora_inicio
 * @property int $hora_fim
 * @property string $status_chamada
 * @property int $unidade_temp_fk
 * @property int $fila_destino
 *
 */
class Usuario extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public $array_hora = [
        '0' => '00',
        '1' => '01',
        '2' => '02',
        '3' => '03',
        '4' => '04',
        '5' => '05',
        '6' => '06',
        '7' => '07',
        '8' => '08',
        '9' => '09',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
    ];
    
    public $array_status = [
        ['id' => '1', 'status' => 'Atendida', 'disposition' => 'ANSWERED'],
        ['id' => '2', 'status' => 'Não atendida', 'disposition' => 'NO ANSWER'],
        ['id' => '3', 'status' => 'Ocupado', 'disposition' => 'BUSY'],
    ];
    
    public static function tableName()
    {
        return 'tb_usuario';
    }
    
    public function rules()
    {
        return [
            [['usuario', 'senha', 'nome', 'email', 'unidade_fk', 'senha_hash', 'auth_key', 'token', 'status', 'unidade_temp_fk'], 'required'],
            [['unidade_fk', 'status', 'hora_inicio', 'hora_fim', 'unidade_temp_fk'], 'integer'],
            [['data_asterisk', 'data_rlt'], 'safe'],
            [['usuario'], 'string', 'max' => 40],
            [['senha', 'senha_hash', 'senha_reset_token', 'token'], 'string', 'max' => 1024],
            [['nome'], 'string', 'max' => 80],
            [['email'], 'string', 'max' => 100],
            [['auth_key'], 'string', 'max' => 32],
            [['voip_origem', 'voip_destino', 'fila_destino'], 'string', 'max' => 30],
            [['status_chamada'], 'string', 'max' => 45],
            [['usuario'], 'unique'],
            [['unidade_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_fk' => 'id']],
            [['unidade_temp_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Unidade::className(), 'targetAttribute' => ['unidade_temp_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Usuário',
            'senha' => 'Senha',
            'nome' => 'Nome',
            'email' => 'e-Mail',
            'unidade_fk' => 'Unidade',
            'senha_hash' => 'Senha Hash',
            'senha_reset_token' => 'Senha Reset Token',
            'auth_key' => 'Auth Key',
            'token' => 'Token',
            'status' => 'Status',
            'data_asterisk' => 'Data Asterisk',
            'data_rlt' => 'Data RLT',
            'voip_origem' => 'VoIP Origem',
            'voip_destino' => 'VoIP Destino',
            'hora_inicio' => 'Hora Início',
            'hora_fim' => 'Hora Fim',
            'status_chamada' => 'Status Chamada',
            'unidade_temp_fk' => 'Unidade Temp',
            'fila_destino' => 'Fila',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->data_asterisk) {
            $this->data_asterisk = Setup::convertApresentacao($this->data_asterisk, 'date');
        }
        if ($this->data_rlt) {
            $this->data_rlt = Setup::convertApresentacao($this->data_rlt, 'date');
        }
    }

    public function afterValidate() {
        parent::afterValidate();
        if ($this->data_asterisk) {
            $this->data_asterisk = Setup::convertBD($this->data_asterisk, 'date');
        }
        if ($this->data_rlt) {
            $this->data_rlt = Setup::convertBD($this->data_rlt, 'date');
        }
    }

    public function getUnidadeFk() {
        return $this->hasOne(Unidade::className(), ['id' => 'unidade_fk']);
    }
    
    public function getUnidadeTempFk() {
        return $this->hasOne(TbUnidade::className(), ['id' => 'unidade_temp_fk']);
    }
    
    public function search($params) {
        $query = Usuario::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_DESC,
                    'unidade_fk' => SORT_ASC,
                    'usuario' => SORT_ASC,
                    'nome' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Usuario']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(nome))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(usuario))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(email))', strtoupper(Setup::retirarAcento($params['Usuario']['pesquisa']))]);
        }
        
        $usuarioperfil = UsuarioPerfil::find()->where(['usuario_fk' => Yii::$app->user->identity->id])->one();
        
        if ($usuarioperfil->perfil_fk != '1') {
            $query->andFilterWhere(['unidade_fk' => Yii::$app->user->identity->unidade_temp_fk]);
        }
        
        return $dataProvider;
    }
    
}
