<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_campo".
 *
 * @property int $id
 * @property string $campo
 * @property string $descricao_campo
 *
 * @property TbUnidade[] $tbUnidades
 */
class Campo extends \yii\db\ActiveRecord
{
    public $pesquisa;
    
    public static function tableName()
    {
        return 'tb_campo';
    }
    
    public function rules()
    {
        return [
            [['campo', 'descricao_campo'], 'required'],
            [['campo'], 'string', 'max' => 40],
            [['descricao_campo'], 'string', 'max' => 255],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campo' => 'Campo',
            'descricao_campo' => 'Descrição Campo',
        ];
    }
    
    public function search($params) {
        $query = Campo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'campo' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Campo']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(campo))', strtoupper(Setup::retirarAcento($params['Campo']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_campo))', strtoupper(Setup::retirarAcento($params['Campo']['pesquisa']))]);
        }

        return $dataProvider;
    }

}
