<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tb_menu".
 *
 * @property int $id
 * @property int $sistema_fk
 * @property int $nivel
 * @property int $menu_pai_fk
 * @property string $titulo
 * @property string $controller
 * @property string $icon
 *
 */
class Menu extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return 'tb_menu';
    }
    
    public function rules()
    {
        return [
            [['sistema_fk', 'nivel', 'titulo'], 'required'],
            [['sistema_fk', 'nivel', 'menu_pai_fk'], 'default', 'value' => null],
            [['sistema_fk', 'nivel', 'menu_pai_fk'], 'integer'],
            [['titulo'], 'string', 'max' => 120],
            [['controller', 'icon'], 'string', 'max' => 200],
            [['menu_pai_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_pai_fk' => 'id']],
            [['sistema_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Sistema::className(), 'targetAttribute' => ['sistema_fk' => 'id']],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sistema_fk' => 'Sistema Fk',
            'nivel' => 'Nivel',
            'menu_pai_fk' => 'Menu Pai Fk',
            'titulo' => 'Titulo',
            'controller' => 'Controller',
            'icon' => 'Icon',
        ];
    }
    
    public function getMenuPaiFk()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_pai_fk']);
    }
    
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['menu_pai_fk' => 'id']);
    }
    
    public function getSistemaFk()
    {
        return $this->hasOne(TbSistema::className(), ['id' => 'sistema_fk']);
    }
}
