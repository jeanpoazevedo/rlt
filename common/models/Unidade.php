<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\components\Setup;

/**
 * This is the model class for table "tb_unidade".
 *
 * @property int $id
 * @property string $unidade
 * @property string $descricao_unidade
 * @property string $bd
 * @property int $campo_fk
 */
class Unidade extends \yii\db\ActiveRecord {
    
    public $pesquisa;

    public static function tableName() {
        return 'tb_unidade';
    }

    public function rules()
    {
        return [
            [['unidade', 'descricao_unidade', 'campo_fk'], 'required'],
            [['campo_fk'], 'integer'],
            [['unidade', 'bd'], 'string', 'max' => 40],
            [['descricao_unidade'], 'string', 'max' => 255],
            [['campo_fk'], 'exist', 'skipOnError' => true, 'targetClass' => Campo::className(), 'targetAttribute' => ['campo_fk' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'unidade' => Yii::t('app', 'Sigla da Unidade'),
            'descricao_unidade' => Yii::t('app', 'Descrição'),
            'bd' => Yii::t('app', 'Banco de Dados'),
            'campo_fk' => 'Campo',
        ];
    }
    
    public function getCampoFk() {
        return $this->hasOne(Campo::className(), ['id' => 'campo_fk']);
    }
    
    public function search($params) {
        $query = Unidade::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'campo_fk' => SORT_ASC,
                    'unidade' => SORT_ASC,
                ]
            ],
        ]);

        if (isset($params['Unidade']['pesquisa'])) {
            $query->orFilterWhere(['like', 'UPPER(fc_remove_acento(unidade))', strtoupper(Setup::retirarAcento($params['Unidade']['pesquisa']))])
                ->orFilterWhere(['like', 'UPPER(fc_remove_acento(descricao_unidade))', strtoupper(Setup::retirarAcento($params['Unidade']['pesquisa']))]);
        }

        return $dataProvider;
    }

}
