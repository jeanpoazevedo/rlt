<?php

namespace common\components;

use Yii;
use common\models\VwRestricaoMenu;

class AccessRulesControl {

    public static function getRulesControl($controller) {
        $actions = [];

        $model = VwRestricaoMenu::findAll(['controller' => $controller, 'usuario_fk' => Yii::$app->user->getId()]);
        foreach ($model as $key => $value) {
            $actions[] = $value->restricao;
        }

        if ($actions) {
            $rule[] = [
                'actions' => $actions,
                'allow' => true,
                'roles' => ['@'],
            ];
        } else {
            $rule[] = [
                'allow' => false,
                'roles' => ['@'],
            ];
        }

        return $rule;
    }

}?>