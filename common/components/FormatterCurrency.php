<?php

namespace common\components;

class FormatterCurrency {

    public static function formatNumber($value, $decimals = 2) {
        if ($value === null)
            return null;
        if ($value === '')
            return '';
        if (strstr($value, ',')) {
            $value = str_replace('.', '', $value);
            $value = str_replace(',', '.', $value);
        }
        return number_format($value, $decimals, ',', '.');
    }

    public static function unformatNumber($formatted_number) {
        if ($formatted_number === null)
            return (float) 0.00;
        if ($formatted_number === '')
            return (float) 0.00;
        if (is_float($formatted_number))
            return $formatted_number;
        $value = str_replace('.', '', $formatted_number);
        $value = str_replace(',', '.', $value);
        return (float) $value;
    }

    public static function truncateNumeric($value, $decimals = 2) {
        $array_value = explode(".", $value);
        return ($array_value[0] . "." . substr($array_value[1], 0, $decimals));
    }

}
