<?php

namespace common\components;

use common\models\Email;

class SendMail {

    public static function submitpassword($emails, $model, $conteudo) {
        $email = Email::findOne(1);
        
        $mailer = \Yii::createObject([
                    'class' => 'yii\swiftmailer\Mailer',
                    'viewPath' => '@common/mail',
                    'useFileTransport' => false,
                    'transport' => [
                        'class' => 'Swift_SmtpTransport',
                        'host' => $email->host,
                        'username' => $email->username,
                        'password' => $email->password,
                        'port' => $email->port,
                        'encryption' => $email->array_encryption[$email->encryption],
                    ]
        ]);

        $mailer->compose([
                    'html' => $conteudo,
                    'text' => 'email_text'
                        ], [
                    'model' => $model
                ])->setFrom([
                    $email->username => $email->nome
                ])->setTo($emails)
                ->setSubject('Atualização senha - RLT')
                ->send();
    }

}
