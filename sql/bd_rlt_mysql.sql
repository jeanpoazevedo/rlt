CREATE DATABASE `bd_rlt` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `bd_rlt`;

CREATE TABLE `tb_sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(1024) NOT NULL,
  `url` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_sistema_id` (`id`),
  KEY `idx_sistema_nome` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(1024) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `port` int(15) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `encryption` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_id` (`id`),
  KEY `idx_email_host` (`host`),
  KEY `idx_email_username` (`username`),
  KEY `idx_email_password` (`password`),
  KEY `idx_email_port` (`port`),
  KEY `idx_email_nome` (`nome`),
  KEY `idx_email_encryption` (`encryption`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(1024) NOT NULL,
  `descricao` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_perfil_id` (`id`),
  KEY `idx_perfil_nome` (`nome`),
  KEY `idx_perfil_descricao` (`descricao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_campo` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `campo` varchar(40) NOT NULL,
  `descricao_campo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_campo_id` (`id`),
  KEY `idx_campo_campo` (`campo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_unidade` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `unidade` varchar(40) NOT NULL,
  `descricao_unidade` varchar(255) NOT NULL,
  `bd` varchar(40) DEFAULT NULL,
  `campo_fk` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_unidade_id` (`id`),
  KEY `idx_unidade_unidade` (`unidade`),
  KEY `idx_unidade_bd` (`bd`),
  CONSTRAINT `fk_unidade_campo`
    FOREIGN KEY (`campo_fk`)
    REFERENCES `tb_campo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_usuario` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `senha` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nome` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `unidade_fk` int(6) NOT NULL,
  `senha_hash` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `senha_reset_token` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `data_asterisk` DATE NULL DEFAULT NULL ,
  `data_rlt` DATE NULL DEFAULT NULL ,
  `voip_origem` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci,
  `voip_destino` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci,
  `hora_inicio` int(6) DEFAULT NULL,
  `hora_fim` int(6) DEFAULT NULL,
  `status_chamada` varchar(45) DEFAULT NULL,
  `unidade_temp_fk` int(6) NOT NULL,
  `fila_destino` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_usuario` (`usuario`),
  KEY `idx_usuario_id` (`id`),
  KEY `idx_usuario_usuario` (`usuario`),
  KEY `idx_usuario_nome` (`nome`),
  KEY `idx_usuario_email` (`email`),
  KEY `idx_usuario_unidade` (`unidade_fk`),
  KEY `idx_usuario_unidade_temp` (`unidade_temp_fk`),
  CONSTRAINT `fk_usuario_unidade`
    FOREIGN KEY (`unidade_fk`)
    REFERENCES `tb_unidade` (`id`),
  CONSTRAINT `fk_usuario_unidade_temp`
    FOREIGN KEY (`unidade_temp_fk`)
    REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_usuario_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_fk` int(11) NOT NULL,
  `perfil_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_usuario_perfil` (`usuario_fk`),
  KEY `idx_usuario_perfil_id` (`id`),
  KEY `idx_usuario_perfil_usuario_fk` (`usuario_fk`),
  KEY `idx_usuario_perfil_perfil_fk` (`perfil_fk`),
  CONSTRAINT `fk_usuario_perfil_usuario` FOREIGN KEY (`usuario_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_usuario_perfil_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sistema_fk` int(11) NOT NULL,
  `nivel` int(11) NOT NULL,
  `menu_pai_fk` int(11) DEFAULT NULL,
  `titulo` varchar(120) NOT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_menu_id` (`id`),
  KEY `idx_menu_sistema_fk` (`sistema_fk`),
  KEY `idx_menu_nivel` (`nivel`),
  KEY `idx_menu_menu_pai_fk` (`menu_pai_fk`),
  KEY `idx_menu_titulo` (`titulo`),
  KEY `idx_menu_controller` (`controller`),
  KEY `idx_menu_icon` (`icon`),
  CONSTRAINT `fk_menu_menu_pai` FOREIGN KEY (`menu_pai_fk`) REFERENCES `tb_menu` (`id`),
  CONSTRAINT `fk_menu_sistema` FOREIGN KEY (`sistema_fk`) REFERENCES `tb_sistema` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_menu_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_fk` int(11) NOT NULL,
  `perfil_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_menu_perfil_id` (`id`),
  KEY `idx_menu_perfil_menu` (`menu_fk`),
  KEY `idx_menu_perfil_perfil` (`perfil_fk`),
  CONSTRAINT `fk_menu_perfil_menu` FOREIGN KEY (`menu_fk`) REFERENCES `tb_menu` (`id`),
  CONSTRAINT `fk_menu_perfil_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_restricao_menu_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_fk` int(11) NOT NULL,
  `menu_fk` int(11) NOT NULL,
  `restricao` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_restricao_menu_perfil_id` (`id`),
  KEY `idx_restricao_menu_perfil_perfil` (`perfil_fk`),
  KEY `idx_restricao_menu_perfil_menu` (`menu_fk`),
  KEY `idx_restricao_menu_perfil_restricao` (`restricao`),
  CONSTRAINT `fk_restricao_menu_perfil_menu` FOREIGN KEY (`menu_fk`) REFERENCES `tb_menu` (`id`),
  CONSTRAINT `fk_restricao_menu_perfil_perfil` FOREIGN KEY (`perfil_fk`) REFERENCES `tb_perfil` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_usuario_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_fk` int(11) NOT NULL,
  `unidade_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_usuario_unidade` (`usuario_fk`,`unidade_fk`),
  KEY `idx_usuario_fk` (`usuario_fk`),
  KEY `idx_unidade_fk` (`unidade_fk`),
  CONSTRAINT `fk_usuario_unidade_usuario` FOREIGN KEY (`usuario_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_usuario_unidade_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_fk` int(11) NOT NULL,
  `queue` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_queue_unidade` (`unidade_fk`,`queue`),
  KEY `idx_unidade_fk` (`unidade_fk`),
  KEY `idx_queue` (`queue`),
  CONSTRAINT `fk_queue_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_queue_agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queue_fk` int(11) NOT NULL,
  `agents` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_queue_agents_queue` (`queue_fk`,`agents`),
  KEY `idx_queue_fk` (`queue_fk`),
  KEY `idx_agents` (`agents`),
  CONSTRAINT `fk_queue_agents_queue` FOREIGN KEY (`queue_fk`) REFERENCES `tb_queue` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_posicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posicao` DATETIME DEFAULT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_cdr_voip` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `usuario_fk` int(11) NOT NULL,
  `unidade_fk` int(11) NOT NULL,
  `calldate` datetime NOT NULL,
  `src` varchar(80) DEFAULT NULL,
  `dst` varchar(80) DEFAULT NULL,
  `dcontext` varchar(80) DEFAULT NULL,
  `atraso` int(11) DEFAULT '0',
  `duration` int(11) DEFAULT '0',
  `billsec` int(11) DEFAULT '0',
  `disposition` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cdr_voip_calldate` (`calldate`),
  KEY `idx_cdr_voip_src` (`src`),
  KEY `idx_cdr_voip_dst` (`dst`),
  KEY `idx_cdr_voip_dcontext` (`dcontext`),
  KEY `idx_cdr_voip_atraso` (`atraso`),
  KEY `idx_cdr_voip_duration` (`duration`),
  KEY `idx_cdr_voip_billsec` (`billsec`),
  KEY `idx_cdr_voip_disposition` (`disposition`),
  CONSTRAINT `fk_cdr_voip_usuario` FOREIGN KEY (`usuario_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_cdr_voip_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tb_cdr_fila` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `usuario_fk` int(11) NOT NULL,
  `unidade_fk` int(11) NOT NULL,
  `calldate` datetime NOT NULL,
  `src` varchar(80) DEFAULT NULL,
  `dst` varchar(80) DEFAULT NULL,
  `atraso` int(11) DEFAULT '0',
  `duration` int(11) DEFAULT '0',
  `billsec` int(11) DEFAULT '0',
  `disposition` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cdr_fila_usuario_fk` (`usuario_fk`),
  KEY `idx_cdr_fila_calldate` (`calldate`),
  KEY `idx_cdr_fila_src` (`src`),
  KEY `idx_cdr_fila_dst` (`dst`),
  KEY `idx_cdr_fila_atraso` (`atraso`),
  KEY `idx_cdr_fila_duration` (`duration`),
  KEY `idx_cdr_fila_billsec` (`billsec`),
  KEY `idx_cdr_fila_disposition` (`disposition`),
  CONSTRAINT `fk_cdr_fila_usuario` FOREIGN KEY (`usuario_fk`) REFERENCES `tb_usuario` (`id`),
  CONSTRAINT `fk_cdr_fila_unidade` FOREIGN KEY (`unidade_fk`) REFERENCES `tb_unidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE
  ALGORITHM = UNDEFINED
  DEFINER = `root`@`localhost`
  SQL SECURITY DEFINER
VIEW `vw_menu` AS
  SELECT
  	`menu`.`id` AS `id`,
  	`user_perfil`.`usuario_fk` AS `usuario_fk`,
  	`menu`.`sistema_fk` AS `sistema_fk`,
  	`menu`.`menu_pai_fk` AS `menu_pai_fk`,
  	`menu`.`nivel` AS `nivel`,
  	`menu`.`titulo` AS `titulo`,
  	`menu`.`controller` AS `controller`,
  	`menu`.`icon` AS `icon`
  FROM
    ((`tb_usuario_perfil` `user_perfil`
  	JOIN `tb_menu_perfil` `menu_perfil` ON ((`menu_perfil`.`perfil_fk` = `user_perfil`.`perfil_fk`)))
  	JOIN `tb_menu` `menu` ON ((`menu`.`id` = `menu_perfil`.`menu_fk`)));

CREATE
  ALGORITHM = UNDEFINED
  DEFINER = `root`@`localhost`
  SQL SECURITY DEFINER
VIEW `vw_restricao_menu` AS
  SELECT
  	`menu`.`id` AS `id`,
    `menu`.`controller` AS `controller`,
    `restricao`.`restricao` AS `restricao`,
    `usuario`.`usuario_fk` AS `usuario_fk`
  FROM
    (((`tb_menu` `menu`
    JOIN `tb_restricao_menu_perfil` `restricao` ON ((`restricao`.`menu_fk` = `menu`.`id`)))
    JOIN `tb_perfil` `perfil` ON ((`perfil`.`id` = `restricao`.`perfil_fk`)))
    JOIN `tb_usuario_perfil` `usuario` ON ((`usuario`.`perfil_fk` = `perfil`.`id`)));