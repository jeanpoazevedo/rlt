/*
Tabela: Sistema
*/
INSERT INTO tb_sistema (nome, url) VALUES
('RLT','http://rlt.system');

/*
Tabela: E-mail
*/
INSERT INTO tb_email (host, username, password, port, nome, encryption) VALUES
('servidor smtp','usuario','senha',587,'nome', '3');

/*
Tabela: Perfil
*/
INSERT INTO tb_perfil (nome, descricao) VALUES
('Administrador','Administrador');

/*
Tabela: Campo
*/
INSERT INTO tb_campo (campo, descricao_campo) VALUES
('USB','União Sul Brasileira'),
('ACP','Associação Central Paranaense');

/*
Tabela: Unidade
*/
INSERT INTO tb_unidade (unidade, descricao_unidade, bd, campo_fk) VALUES
('Sede','Sede Administrativa','dbUsb', '1'),
('Sede','Sede Administrativa','dbAcp', '2');

/*
Tabela: Usuario
*/
INSERT INTO tb_usuario (usuario, senha, nome, email, unidade_fk, status, senha_hash, senha_reset_token, auth_key, token, data_asterisk, data_rlt, unidade_temp_fk) VALUES
('admin','$2y$13$V4tiTCp5lc0COnoHBHKnLeqP9sdtUIEE.3FDOeLO5hhIjgnGUJ64u','Administrador','email@email.com.br',1,1,'$2y$13$V4tiTCp5lc0COnoHBHKnLeqP9sdtUIEE.3FDOeLO5hhIjgnGUJ64u',NULL,'BSVNWgWwVS-z5XQLH7I39Qr65zATAEbq','MSBFsg5GlAT5_oXMzpyBWgRuGS2ADwZM','2000-01-01','2000-01-01',1);

/*
Tabela: Usuario Perfil
*/
INSERT INTO tb_usuario_perfil (usuario_fk, perfil_fk) VALUES
(1,1);

/*
Tabela: Menu
*/
INSERT INTO tb_menu (sistema_fk, nivel, menu_pai_fk, titulo, controller, icon) VALUES
(1,0,NULL,'Relatórios',NULL,'glyphicon glyphicon-phone-alt'),
(1,0,NULL,'Fila',NULL,'fa fa-cog'),
(1,0,NULL,'Configuração',NULL,'fa fa-cog'),
(1,1,1,'On-Line','online','md md-call'),
(1,1,1,'Off-Line','offline','md md-call'),
(1,1,1,'Fila','fila','md md-call'),
(1,1,2,'Fila','queue','md md-call'),
(1,1,2,'Menbro da Fila','agentsqueue','md md-call'),
(1,1,3,'Usuário','usuario','icon icon-user '),
(1,1,3,'Campo','campo','md md-location-city'),
(1,1,3,'Unidade','unidade','md md-location-city'),
(1,1,3,'Perfil','perfil','fa fa-users'),
(1,1,3,'Usuário Unidade','userunidade','md md-location-city'),
(1,1,3,'Usuário Perfil','profileusuario','fa fa-users'),
(1,1,3,'e-Mail','email','md md-mail'),
(1,1,3,'Sistema','system','ion ion-settings');

/*
Tabela: Menu Perfil - Master
*/
INSERT INTO tb_menu_perfil (menu_fk, perfil_fk) VALUES
(1,1),
(2,1),
(3,1),
(4,1),
(5,1),
(6,1),
(7,1),
(8,1),
(9,1),
(10,1),
(11,1),
(12,1),
(13,1),
(14,1),
(15,1),
(16,1);

/*
Tabela: Restricao Menu Perfil - Master
*/
INSERT INTO tb_restricao_menu_perfil (perfil_fk, menu_fk, restricao) VALUES
(1,4,'index'),
(1,4,'clean'),
(1,5,'index'),
(1,5,'clean'),
(1,6,'index'),
(1,6,'clean'),
(1,6,'filtro'),
(1,7,'index'),
(1,7,'create'),
(1,7,'update'),
(1,7,'delete'),
(1,8,'index'),
(1,8,'create'),
(1,8,'update'),
(1,8,'delete'),
(1,9,'index'),
(1,9,'create'),
(1,9,'update'),
(1,9,'view'),
(1,9,'delete'),
(1,9,'password'),
(1,10,'index'),
(1,10,'create'),
(1,10,'update'),
(1,10,'view'),
(1,10,'delete'),
(1,11,'index'),
(1,11,'create'),
(1,11,'update'),
(1,11,'view'),
(1,11,'delete'),
(1,12,'index'),
(1,12,'create'),
(1,12,'update'),
(1,12,'delete'),
(1,12,'view'),
(1,12,'menu'),
(1,12,'restrict'),
(1,13,'index'),
(1,13,'create'),
(1,13,'update'),
(1,13,'view'),
(1,13,'delete'),
(1,14,'index'),
(1,14,'create'),
(1,14,'update'),
(1,14,'view'),
(1,14,'delete'),
(1,15,'index'),
(1,15,'update'),
(1,15,'view'),
(1,16,'index'),
(1,16,'update'),
(1,16,'view');

/*
Tabela: Usuario Unidade
*/
INSERT INTO tb_usuario_unidade (usuario_fk, unidade_fk)VALUES
(1,1);

/*
Tabela: Posicao
*/
INSERT INTO `tb_posicao` (`id`, `posicao`) VALUES ('1', '2000-01-01');

